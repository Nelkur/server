import jwt from "jsonwebtoken";
import fs from "fs";
import __dirname from "../utils/dirname.js";

const PUB_KEY = fs.readFileSync(`${__dirname}/../keys/id_rsa_pub.pem`, "utf8");
const PRIV_KEY = fs.readFileSync(
  `${__dirname}/../keys/id_rsa_priv.pem`,
  "utf8"
);

class Jwt {
  static generateToken(payload) {
    const token = jwt.sign(payload, PRIV_KEY, {
      expiresIn: "7d",
      algorithm: "RS256",
    });
    return token;
  }

  static validateToken(token) {
    const payload = jwt.verify(
      token,
      PUB_KEY,
      {
        expiresIn: "7d",
        algorithm: ["RS256"],
      },
      (err, payload) => {
        if (err) throw err;
        return payload;
      }
    );
    return payload;
  }
}

export default Jwt;
