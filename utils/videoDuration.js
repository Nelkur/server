function YTDurationToSeconds(duration) {
  var match = duration.match(/PT(\d+H)?(\d+M)?(\d+S)?/);

  match = match.slice(1).map(function (x) {
    if (x != null) {
      return x.replace(/\D/, "");
    }
  });

  var hours = parseInt(match[0]) || 0;
  var minutes = parseInt(match[1]) || 0;
  var seconds = parseInt(match[2]) || 0;

  return { hours, minutes, seconds };
}

export default function (duration) {
  const t = YTDurationToSeconds(duration);
  var videoDuration = "";
  if (t.hours !== 0) {
    videoDuration += t.hours && t.hours < 10 ? `0${t.hours}:` : t.hours;
  }
  if (t.minutes === 0) {
    videoDuration += "00";
  } else {
    videoDuration +=
      t.minutes && t.minutes < 10 ? `0${t.minutes}:` : `${t.minutes}:`;
  }
  if (t.seconds === 0) {
    videoDuration += "00";
  } else {
    videoDuration += t.seconds < 10 ? `0${t.seconds}` : `${t.seconds}`;
  }

  return videoDuration;
}
