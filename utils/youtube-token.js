export const TOKEN_DIR =
  (process.env.HOME || process.env.HOMEPATH || process.env.USERPROFILE) +
  "/.credentials/";

export const TOKEN_PATH = TOKEN_DIR + "eschooling-youtube.json";
