import crypto from "crypto";
// crypto.DEFAULT_ENCODING = 'hex';
const iterations = 10594;
const keylen = 256;
const digest = "sha512";

export const generateSalt = function () {
  return new Promise((resolve, reject) => {
    crypto.randomBytes(24, (err, buff) => {
      if (err) {
        reject();
        throw err;
      }
      return resolve(buff.toString("hex"));
    });
  });
};

export const hash = function (password, salt) {
  return new Promise((resolve, reject) => {
    crypto.pbkdf2(
      password,
      salt,
      iterations,
      keylen,
      digest,
      (err, derivedKey) => {
        if (err) {
          reject();
          throw err;
        }
        return resolve(derivedKey.toString("hex"));
      }
    );
  });
};

// The crypto.DEFAULT_ENCODING property can be used to change the way the derivedKey is passed to the callback. This property, however, has been deprecated and use should be avoided.
