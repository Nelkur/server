import Slug from "slugify";

// Matches slugs that follow the format slug, slugs-1, slugs-2 etc
export function SlugRegex(slug) {
  return new RegExp(`^(${slug})((-[0-9]*$)?)$`, "i");
}

export function Slugify(str) {
  // return Slug(str, { remove: /[*+~.()'\/"!,#:@\\]/g, lower: true });
  return Slug(str, {
    replacement: "-",
    remove: /[*+~.()'"!:@]/g,
    lower: true,
    strict: true,
  });
}
