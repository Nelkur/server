import randomize from "./randomize.js";

const subjectCodes = [
  {
    subject: "mathematics",
    code: "MT",
  },
  {
    subject: "english",
    code: "EN",
  },
  {
    subject: "kiswahili",
    code: "KS",
  },
  {
    subject: "science",
    code: "SC",
  },
  {
    subject: "social studies",
    code: "SS",
  },
  {
    subject: "cre",
    code: "CR",
  },
  {
    subject: "ire",
    code: "IR",
  },
];

export function generateCode(subject, type) {
  const code = subjectCodes.filter((s) => s.subject === subject);
  const suffix = type === "exam" ? "E" : "T";
  const random = randomize(1, 9999);
  return `${code[0].code}${suffix}${random}`;
}

export default {
  generateCode,
};
