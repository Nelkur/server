import randomize from "./randomize.js";

function getRandomString(length) {
  const randomChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  let result = "";
  for (let i = 0; i < length; i++) {
    result += randomChars.charAt(
      Math.floor(Math.random() * randomChars.length)
    );
  }
  return result;
}

export function generateCode(name) {
  const random = randomize(1, 9999);
  const matches = name.match(/\b(\w)/g);
  const acronym = matches.join("");
  let str = "";
  if (matches.length < 2) {
    const letter = getRandomString(1);
    const letter2 = getRandomString(1);
    str = `${acronym}${letter}${letter2}`.toUpperCase();
    return `${str}-${random}`;
  } else if (matches.length < 3) {
    const letter = getRandomString(1);
    str = `${acronym}${letter}`.toUpperCase();
    return `${str}-${random}`;
  }
  str = `${acronym}`.toUpperCase();
  return `${str}-${random}`;
}
