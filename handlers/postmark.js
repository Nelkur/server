import * as postmark from "postmark";
import pug from "pug";
import htmlToText from "html-to-text";
import juice from "juice";

let client = new postmark.ServerClient(process.env.POSTMARK_API_TOKEN);

const generateHTML = (filename, options = {}) => {
  const html = pug.renderFile(
    `${__dirname}/../views/email/${filename}.pug`,
    options
  );
  const inlineHtml = juice(html);
  return inlineHtml;
};

class Mail {
  static async send(options) {
    const html = generateHTML(options.filename, options);
    const text = htmlToText.fromString(html);

    const mailOptions = {
      From: "Eschooling <welcome@eschooling.co.ke>",
      To: options.user.email,
      Subject: options.subject,
      TextBody: text,
      HtmlBody: html,
    };

    return client.sendEmail(mailOptions);
  }
}

export default Mail;
