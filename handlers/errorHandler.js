import Joi from "@hapi/joi";
const ValidationError = Joi.ValidationError;
/*
  Catch Errors Handler

  With async/await, you need some way to catch errors
  Instead of using try{} catch(e) {} in each controller, we wrap the function in
  catchErrors(), catch any errors they throw, and pass it along to our express middleware with next()
*/

export const catchErrors = (fn) => {
  return function (req, res, next) {
    return fn(req, res, next).catch(next);
  };
};

/*
  Not Found Error Handler
  If we hit a route that is not found, we mark it as 404 and pass it along to the next error handler to display
*/
export const notFound = (req, res, next) => {
  const err = new Error("Not Found");
  err.status = 404;
  next(err);
};

/*
  Development Error Handler

  In development we show good error messages so if we hit a syntax error or any other previously un-handled error, we can show good info on what happened
*/
export const developmentErrors = (err, req, res, next) => {
  if (err instanceof ValidationError) {
    err.status = 400;
  }
  const validationError = err.details ? err.details[0].message : "";
  err.stack = err.stack || "";
  const errorDetails = {
    message: validationError || err.message,
    status: err.status,
    stackHighlighted: err.stack.replace(
      /[a-z_-\d]+.js:\d+:\d+/gi,
      "<mark>$&</mark>"
    ),
  };
  res.status(err.status || 500);
  // logger.error(errorDetails.message);
  console.log(err);
  // console.log(errorDetails);
  res.format({
    // Based on the `Accept` http header
    "text/html": () => {
      res.render("error", errorDetails);
    }, // Form Submit, Reload the page
    "application/json": () => res.json(errorDetails), // Ajax call, send JSON back
  });
};

/*
  Production Error Handler
  No stacktraces are leaked to user
*/
export const productionErrors = (err, req, res, next) => {
  if (err instanceof ValidationError) {
    err.status = 400;
  }
  const validationError = err.details ? err.details[0].message : "";
  res.status(err.status || 500);
  // logger.error(err.message);
  res.render("error", {
    message: validationError || err.message,
    error: {},
  });
};
