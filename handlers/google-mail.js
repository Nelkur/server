import nodemailer from "nodemailer";
import * as key from "../gsuite_auth.js";

const SENDER_EMAIL = "support@domain.com";
const RECEIVER_EMAIL = "customer@gmail.com";

async function start() {
  const transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 465,
    secure: true,
    auth: {
      type: "OAuth2",
      user: SENDER_EMAIL,
      serviceClient: "114228948003924985221",
      privateKey:
        "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDl0e4hODDH9Y/S\noN1QWjjidm+fs8TfvSophS3USAT86ej+9YEFZ6HrJMPiBiqU7JQ8U68W7wb7NFFI\n4lIuoFeSyB9rZY60IsStEE/IRUmYnZMo0zhz61yh126iTMM3Ls+GOUwVUtGNDk6S\nUbsHFrYgoCC/dArI/u9Lndxw6tAoWe/8J/fY0ATm6jxoaZpA958MQysVuC+1RK1m\nZuugLsJyXgIrfkl2IcdKihyhAHoxlPG0s2yNaxI7OirbFYznAND3zcWJp8r170u7\nLxNJvJk9Nj8TApjJr0mmGtaN0koJsQoNuJjRZC4s7ZgQ91pk4vS8ifQ+u4GAbCZJ\n9qHYtuFJAgMBAAECggEACAM3oHMJ/lq70DM5bBpX1otxt67H69NiTi7IFYPfn/q+\nxm7ZoqOFlBdveJNQS5TA/qZzRGMtNOJoKBid7BASkDnyddHatcHq5gFw1VHBxQn6\nDhJpKt93QWbrrZ8Wr6Ht7HyAatOSSJmfAyp0Jm/esIisrkggh81IRgm5BxB6FUIB\nOBslKmWmOsi6oxqa1rJzju3F3cSu85uzZhvLJT9SjT7YdAwuB7sR68YeDIaOBUkq\n5Ke9QLs96+AjE3CesUoRQcGQdZtnPfJTAmn/wyax/7eGtpIWMj4vtgMUqNspNMm3\nH/LK4dc5F9v7jh04lOceg2iwZjNgkZLbTnXd2SE9oQKBgQD6gOEt5ogAIhDx+7Ea\nMIUeJtO91oN7h8TDYKwLIsr/6jjj56sUtWMzzwqXLJAEbo6nyFpQFEBWayY7I4Cz\nlU6A1tDiIQEHXAiqyVb0Vtyji8dZqMSRifTED6KvMq6lpOWutLBFK2KFvXZZY4K6\nWqaS+3IkmEZQSHygGg9XIgNx8QKBgQDq3N5Rvg3zEwAtV1wHGlbuk8cVPE5TqSVP\nopwfKWkfeE/zbmm4zQ65G5HXQ8Pu7ATlb0do9kJY8Z4h+gk5WMgqL6EFJpUY9FZH\n1AgSkfwEn3BSue8RHj33r8t7dTCTYPwHZx8SLLjBg3v6jmiRf5VqZ/ijmmuCD2nz\nHBAAjbQM2QKBgCWanzLyvku20XBh5g4WZoVGMtb2krpCgUkLBAiP6oVMNcoQhuAO\n8pteYyqaZ+wiZIvPSa9nuti+MPkDtVg/+47fsG+hoLPoHuz0WHdJasmqO/y7lvZ6\nDSI5bqR6KTl4bXoyaZuYjauh61gz1fX/ka3SzSmtKjD3EK4/+zmLQ5UhAoGBANGY\nJjcguuYwELpl+dTE5sumycXjuarjik6VOWp91DincAI4iNTI+tirB+3ppdN0gbIX\nKs06517k0VBVCSJKXud29eXY1rocY1tal5GrAhNGDkFleTn3mSmPkrIslRsRNBmP\nJ6Vaj87yfhPOPveJulfpTXUDe+nnw4+XfZhg3UuRAoGAWItzek9+3ysPcp/NWPqd\n8xF5bcLucwmZrXc4U44MOf8wBfEtvc2K4r1FT2t/mpUfkLUk1ypxPOhn2KYkw1L5\naAgx4n9K2iDAXG9v8pt0mkWaz64Q8S0DyyD5xyiClEaCGh0eIDTmI7T2CpW+jQ0d\n4qfEfYBRRTbiIEJkQOWWkd0=\n-----END PRIVATE KEY-----\n",
    },
  });

  try {
    await transporter.verify();
    await transporter.sendMail({
      from: SENDER_EMAIL,
      to: RECEIVER_EMAIL,
      subject: "Hello Humans",
      text: "The quick brown fox jumps over the lazy dog.",
    });
  } catch (err) {
    console.error(err);
  }
}

export default start;
