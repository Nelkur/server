import nodemailer from "nodemailer";
import pug from "pug";
import htmlToText from "html-to-text";
import juice from "juice";

import { dirname } from "path";
import { fileURLToPath } from "url";
const __dirname = dirname(fileURLToPath(import.meta.url));

// Create reusable transporter object using the default SMTP transport
let transporter = nodemailer.createTransport({
  host: process.env.MAIL_HOST,
  port: process.env.MAIL_PORT,
  secure: false,
  auth: {
    user: process.env.MAIL_USER,
    pass: process.env.MAIL_PASS,
  },
});

const generateHTML = (filename, options = {}) => {
  const html = pug.renderFile(
    `${__dirname}/../views/email/${filename}.pug`,
    options
  );
  const inlineHtml = juice(html);
  return inlineHtml;
};

class Mail {
  static async send(options) {
    const html = generateHTML(options.filename, options);
    const text = htmlToText.fromString(html);

    const mailOptions = {
      from: "Eschooling <noreply@eschooling.co.ke>",
      to: options.user.email,
      subject: options.subject,
      html,
      text,
    };

    return transporter.sendMail(mailOptions);
  }
}

export default Mail;
