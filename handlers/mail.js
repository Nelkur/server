import mailtrap from "./mailtrap.js";
// import postmark from "./postmark.js";
import gsuite from "./gsuite.js";
// import start from "./google-mail.js";
// start();

let Mail;

Mail = process.env.NODE_ENV === "development" ? gsuite : mailtrap;

export default Mail;
