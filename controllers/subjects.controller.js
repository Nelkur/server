import mongoose from "mongoose";
const Subject = mongoose.model("Subject");
import { SubjectValidationSchema } from "../models/Subjects.js";

class SubjectsController {
  static async getAll(req, res) {
    const subjects = await Subject.find();
    res.json(subjects);
  }

  static async addNew(req, res) {
    await SubjectValidationSchema.validateAsync(req.body);
    const subject = new Subject(req.body);
    await subject.save();

    res.json(subject);
  }
}

export default SubjectsController;
