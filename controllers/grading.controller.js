import mongoose from "mongoose";
import { GradeValidationSchema } from "../models/Grading.js";
const Grading = mongoose.model("Grading");

class GradingController {
  static async getAll(req, res) {
    const grading = await Grading.find();
    res.json(grading);
  }

  static async getOne(req, res) {
    const grading = await Grading.find({ system: req.params.system });
    res.json(grading);
  }

  static async addNew(req, res) {
    await GradeValidationSchema.validateAsync(req.body);
    const grading = new Grading(req.body);
    await grading.save();
    res.json(grading);
  }
}

export default GradingController;
