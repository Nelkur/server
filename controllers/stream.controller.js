import axios from "axios";
import crypto from "crypto";

class StreamController {
  static async generateSignature(req, res) {
    const { meetingNumber, role } = req.body;
    const timestamp = new Date().getTime() - 30000;
    const msg = Buffer.from(
      process.env.ZOOM_KEY + meetingNumber + timestamp + role
    ).toString("base64");
    const hash = crypto
      .createHmac("sha256", process.env.ZOOM_SECRET)
      .update(msg)
      .digest("base64");
    const signature = Buffer.from(
      `${process.env.ZOOM_KEY}.${meetingNumber}.${timestamp}.${role}.${hash}`
    ).toString("base64");

    res.json({
      signature: signature,
    });
  }

  static async createMeeting(req, res) {
    const { data } = await axios({
      method: "post",
      url: "https://api.zoom.us/v2/users/eschooling.ke@gmail.com/meetings",
      headers: { Authorization: `Bearer ${process.env.ZOOM_TOKEN}` },
      data: req.body,
    });
    console.log("Meeting data");
    console.log(data);
    res.json(data);
  }
}

export default StreamController;
