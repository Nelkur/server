import mongoose from "mongoose";
import moment from "moment";
const Payment = mongoose.model("Payment");
const Subscription = mongoose.model("Subscription");

import axios from "axios";
// Authenticate your app - Step 1
const auth = new Buffer.from(
  `${process.env.CONSUMER_KEY}:${process.env.CONSUMER_SECRET}`
).toString("base64");
const authString = `Basic ${auth}`;
// Process request
const timestamp = moment().format("YYYYMMDDhhmmss");
const password = new Buffer.from(
  `${process.env.MPESA_SHORTCODE}${process.env.PASSKEY}${timestamp}`
).toString("base64");

const getAuthToken = () => {
  return axios({
    method: "get",
    url: "https://sandbox.safaricom.co.ke/oauth/v1/generate",
    params: {
      grant_type: "client_credentials",
    },
    headers: { Authorization: authString },
  });
};

const initialPayload = (data) => {
  const ext =
    data.teachers && data.students ? `${data.teachers}/${data.students}` : "";

  const pload = {
    BusinessShortCode: process.env.MPESA_SHORTCODE,
    Password: password,
    Timestamp: timestamp,
    TransactionType: "CustomerPayBillOnline",
    Amount: data.amount,
    PartyA: data.phone,
    PartyB: process.env.MPESA_SHORTCODE,
    PhoneNumber: data.phone,
    CallBackURL: `${process.env.HOST}/payments/response/${data.subscription}/${ext}`,
    AccountReference: "Elimu",
    TransactionDesc: data.subscription,
  };

  return JSON.stringify(pload);
};

const modelPaymentPayload = (
  data,
  user,
  subscriptionType,
  teachers,
  students
) => {
  const {
    MerchantRequestID,
    CheckoutRequestID,
    ResultCode,
    ResultDesc,
    CallbackMetadata,
  } = data;
  const payload = {
    type: "stkPush",
    accountType: user.type,
    accountId: user.id,
    subscriptionType,
    teachers,
    students,
    merchantRequestId: MerchantRequestID,
    checkoutRequestId: CheckoutRequestID,
    resultCode: ResultCode,
    resultDesc: ResultDesc,
    callbackMetadata: processCallbackMetadata(CallbackMetadata.Item),
  };

  return payload;
};

const processCallbackMetadata = (data) => {
  const results = data.reduce((result, item) => {
    if (!result[item["Name"]]) {
      result[item["Name"]] = "";
    }
    result[item["Name"]] = item["Value"] || "";

    return result;
  }, {});

  const payload = {
    amount: results.Amount,
    mpesaReceiptNumber: results.MpesaReceiptNumber,
    balance: results.Balance,
    transactionDate: createJsDate(results.TransactionDate),
    phoneNumber: results.PhoneNumber,
  };

  return payload;
};

const createJsDate = (date) => {
  const items = date.toString().split("");
  const year = items[0] + items[1] + items[2] + items[3];
  const month = parseFloat(items[4] + items[5]) - 1;
  const day = items[6] + items[7];
  const hour = items[8] + items[9];
  const minutes = items[10] + items[11];
  const seconds = items[12] + items[13];

  return new Date(year, month, day, hour, minutes, seconds);
};

const subscriptionPayload = (user, payment, expiry) => {
  return JSON.stringify({
    accountType: user.type,
    accountId: user.id,
    subscriptionType: payment.subscription,
    teachers: payment.teachers,
    students: payment.students,
    paymentId: payment._id,
    paymentMethod: "stkPush",
    amount: payment.amount,
    description: "new subscription",
    status: "active",
    expiry,
  });
};

const getExpiry = (subType) => {
  let t;
  if (subType === "monthly") {
    t = 1;
  } else if (subType === "termly") {
    t = 4;
  } else if (subType === "annual") {
    t = 12;
  } else {
    t = 0;
  }
  return t;
};

class PaymentsController {
  static async processRequest(req, res) {
    const payload = initialPayload(req.body);
    const token = await getAuthToken();
    const result = await axios({
      method: "post",
      url: "https://sandbox.safaricom.co.ke/mpesa/stkpush/v1/processrequest",
      headers: {
        Authorization: `Bearer ${token.data.access_token}`,
        "Content-Type": "application/json",
      },
      data: payload,
    });
    console.log(result.data);
    res.json(result.data);
  }

  static async getResponse(req, res) {
    console.log("------------ Received Mpesa Webhook --------------");
    if (req.body.Body.stkCallback.CallbackMetadata) {
      // Payment was successful
      const teachers = req.params.teachers ? req.params.teachers : undefined;
      const students = req.params.students ? req.params.students : undefined;
      const payload = modelPaymentPayload(
        req.body.Body.stkCallback,
        req.user,
        req.params.subscription,
        teachers,
        students
      );
      const payment = await new Payment(payload);
      await payment.save();
      console.log("--- Successfully posted to database ---");
      console.log(payment);
      // Save to your database and return
      res.json(true);
    }
    res.json(false);
  }

  static async confirmPayment(req, res) {
    const payment = await Payment.aggregate([
      {
        $match: {
          type: "stkPush",
          "callbackMetadata.mpesaReceiptNumber": req.body.id,
        },
      },
      {
        $project: {
          transactionId: "$callbackMetadata.mpesaReceiptNumber",
          amount: "$callbackMetadata.amount",
          subscription: "$subscriptionType",
          teachers: "$teachers",
          students: "$students",
        },
      },
      { $limit: 1 },
    ]);

    // If successfully confirmed - POST to subsription model
    if (payment) {
      const sub = await Subscription.findOne({ accountId: req.user.id });
      let startTime;
      let expiry;
      if (sub) {
        startTime = sub.status === "active" ? sub.expiry : new Date();
      } else {
        startTime = new Date();
      }

      const subscription = payment.subscription;
      if (subscription === "weekly") {
        expiry = moment(startTime).add(7, "days").format();
      } else {
        let t = getExpiry(subscription);
        expiry = moment(startTime).add(t, "months").format();
      }

      if (sub) {
        sub.subscriptionType = subscription;
        sub.teachers = payment.teachers ? payment.teachers : undefined;
        sub.students = payment.students ? payment.students : undefined;
        sub.expiry = expiry;
        sub.status = "active";
        sub.description = "renewal";
        sub.paymentId = payment._id;
        sub.updated = new Date();
        await sub.save();

        res.json(sub);
      } else {
        const payload = subscriptionPayload(req.user, payment, expiry);
        const subscription = new Subscription(payload);
        await subscription.save();
        res.json(payload);
      }
    }

    res.json(payment);
  }

  static async getAllPayments(req, res) {
    const payments = await Payment.find(
      { accountId: req.user.id },
      {
        "callbackMetadata.mpesaReceiptNumber": 1,
        type: 1,
        subscriptionType: 1,
        "callbackMetadata.amount": 1,
        "callbackMetaData.transactionDate": 1,
      }
    ).sort({ "callbackMetaData.transactionDate": -1 });
    res.json(payments);
  }

  static async getSubscription(req, res) {
    const subscription = await Subscription.findOne({ accountId: req.user.id });
    res.json(subscription);
  }
}

export default PaymentsController;
