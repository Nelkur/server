import mongoose from "mongoose";
const Institution = mongoose.model("Institution");
const Teacher = mongoose.model("Teacher");
const Student = mongoose.model("Student");
const Content = mongoose.model("Content");
import {
  InstitutionValidationSchema,
  LoginValidationSchema,
  GradesValidationSchema,
} from "../models/Institutions.js";
import fs from "fs";
import pick from "lodash.pick";
import omit from "lodash.omit";
import { hash } from "../utils/hashes.js";
import Mail from "../handlers/mail.js";

class InstitutionController {
  static async getAll(req, res) {
    const institutions = await Institution.find();
    res.json(institutions);
  }

  static async getOne(req, res) {
    const institution = await Institution.find(
      { slug: req.params.slug },
      { salt: 0, password: 0 }
    );
    res.json(institution);
  }

  static async getCurrentInstitution(req, res) {
    const user = await Institution.find(
      { _id: req.user.id },
      { salt: 0, password: 0 }
    );
    res.json(user);
  }

  static async uploadPhoto(req, res) {
    const { currentPhoto } = req.body;
    if (currentPhoto !== "undefined") {
      fs.unlink(`./public/uploads/users/${currentPhoto}`, (err) => {
        if (err) {
          throw err;
        }
      });
    }
    const doc = await Instituiton.findOne({ _id: req.user.id });
    doc.photo = req.body.photo;
    await doc.save();

    res.json(omit(doc, ["salt", "password"]));
  }

  static async updateInstitution(req, res) {
    const doc = await Institution.findOne({ slug: req.params.slug });
    Object.entries(req.body).forEach(([key, value]) => {
      doc[key] = value;
    });
    await doc.save();

    res.json(omit(doc, ["salt", "password"]));
  }

  static async addNew(req, res) {
    await InstitutionValidationSchema.validateAsync(req.body);

    let institution = await Institution.findOne({ email: req.body.email });
    if (institution)
      throw { status: 400, message: "Institution is already registered!" };
    const payload = omit(req.body, ["confirmPassword"]);

    institution = new Institution(payload);
    await institution.save();

    const token = institution.generateAuthToken();
    res
      .header("x-auth-token", token)
      .header("access-control-expose-headers", "x-auth-token")
      .json(
        pick(institution, [
          "_id",
          "type",
          "system",
          "name",
          "slug",
          "phone",
          "email",
          "adminName",
        ])
      );
  }

  static async login(req, res) {
    await LoginValidationSchema.validateAsync(req.body);

    const user = await Institution.findOne({ email: req.body.email });
    if (!user) throw { status: 400, message: "Invalid email or password" };

    const confirmPassword = await hash(req.body.password, user.salt);
    if (confirmPassword !== user.password)
      throw { status: 400, message: "Invalid email or password" };

    const token = user.generateAuthToken();
    res.json(token);
  }

  static async getAllTeachers(req, res) {
    const teachers = await Teacher.find({ school: req.user.id });
    res.json(teachers);
  }

  static async addTeacher(req, res) {
    const teachers = await Teacher.insertMany(req.body.teachers);
    teachers.map(async (teacher) => {
      const registerUrl = `${process.env.CLIENT_URL}/teachers/${teacher._id}`;
      await Mail.send({
        user: teacher,
        filename: "invite-teacher",
        subject: "Teacher Registration",
        registerUrl,
      });
    });
    res.json(teachers);
  }

  static async updateTeacher(req, res) {
    const doc = await Teacher.findOne({ _id: req.params.id });
    Object.entries(req.body).forEach(([key, value]) => {
      doc[key] = value;
    });
    await doc.save();

    res.json(doc);
  }

  static async deleteTeacher(req, res) {
    const teacher = await Teacher.deleteOne({ _id: req.params.id });
    res.json(teacher);
  }

  static async getTeacherContent(req, res) {
    const content = await Content.find({ author: req.params.id });
    res.json(content.length);
  }

  static async getGrading(req, res) {
    const grading = await Institution.aggregate([
      { $match: { _id: req.params.school } },
      { $project: { grading: 1, system: 1 } },
    ]);

    res.json(grading[0]);
  }

  static async setGrading(req, res) {
    await GradesValidationSchema.validateAsync(req.body);
    const institution = await Institution.findOne({ _id: req.user.id });
    institution.grading = req.body;
    await institution.save();

    res.json(institution);
  }

  static async getAllStudents(req, res) {
    const students = await Student.find({ school: req.user.id });
    res.json(students);
  }
}

export default InstitutionController;
