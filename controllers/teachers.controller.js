import fs from "fs";
import omit from "lodash.omit";
import mongoose from "mongoose";
import bson from "bson";
const ObjectId = bson.ObjectId; // ObjectId(my_id_string)
const Teacher = mongoose.model("Teacher");
const Student = mongoose.model("Student");
const Content = mongoose.model("Content");
const Results = mongoose.model("Result");
const Notes = mongoose.model("Notes");
const Videos = mongoose.model("Videos");
import { LoginValidationSchema } from "../models/Teachers.js";
import { hash } from "../utils/hashes.js";
import { generateCode } from "../utils/studentCode.js";

class TeachersController {
  static async getAll(req, res) {
    const teachers = await Teacher.find();
    res.json(teachers);
  }

  static async getOne(req, res) {
    const teacher = await Teacher.findOne({ _id: req.user.id }).populate(
      "school",
      "name playlistId"
    );
    res.json(omit(teacher, ["password", "salt"]));
  }

  static async registerTeacherLanding(req, res) {
    const isValid = mongoose.Types.ObjectId.isValid(req.params.id);
    if (isValid) {
      const doc = await Teacher.findOne({ _id: req.params.id });
      if (!doc)
        throw { status: 400, message: "Sorry that account does not exist!" };
      res.json(omit(doc, ["salt", "password"]));
    } else {
      throw { status: 400, message: "Invalid Id" };
    }
  }

  static async registerTeacher(req, res) {
    const doc = await Teacher.findOne({ _id: req.params.id });
    Object.entries(req.body).forEach(([key, value]) => {
      doc[key] = value;
    });
    await doc.save();

    const token = doc.generateAuthToken();
    res
      .header("x-auth-token", token)
      .header("access-control-expose-headers", "x-auth-token")
      .json(omit(doc, ["salt", "password"]));
  }

  static async uploadPhoto(req, res) {
    const { currentPhoto } = req.body;
    if (currentPhoto !== "undefined") {
      fs.unlink(`./public/uploads/users/${currentPhoto}`, (err) => {
        if (err) {
          throw err;
        }
      });
    }
    const doc = await Teacher.findOne({ _id: req.user.id });
    doc.photo = req.body.photo;
    await doc.save();

    res.json(omit(doc, ["salt", "password"]));
  }

  static async updateTeacher(req, res) {
    const doc = await Teacher.findOne({ _id: req.user.id });
    Object.entries(req.body).forEach(([key, value]) => {
      doc[key] = value;
    });
    await doc.save();

    res.json(omit(doc, ["salt", "password"]));
  }

  static async login(req, res) {
    await LoginValidationSchema.validateAsync(req.body);

    const teacher = await Teacher.findOne({ email: req.body.email });
    if (!teacher) throw { status: 400, message: "Invalid email or password" };

    const confirmPassword = await hash(req.body.password, teacher.salt);
    if (confirmPassword !== teacher.password)
      throw { status: 400, message: "Invalid email or password" };

    const token = teacher.generateAuthToken();
    res.json(token);
  }

  static async addStudent(req, res) {
    const payload = req.body.map((student) => {
      student.code = generateCode(student.name);
      return student;
    });
    const students = await Student.insertMany(payload);
    console.log(students);
    res.json(students);
  }

  // Only for class admins
  static async getAllStudents(req, res) {
    const students = await Student.find({
      school: req.body.id,
      class: req.body.class,
    });
    res.json(students);
  }

  static async deleteStudent(req, res) {
    const teacher = await Student.deleteOne({ _id: req.params.id });
    res.json(teacher);
  }

  // Get the students that did a certain paper
  static async getResults(req, res) {
    const results = await Results.find(req.body).populate("user");
    console.log;
    const average =
      results.reduce((total, next) => total + next.percentage, 0) /
      results.length;

    res.json({ results, average });
  }

  static async deleteResult(req, res) {
    const doc = await Results.deleteOne({ _id: req.params.id });
    res.json(doc);
  }

  static async getAllMyPapers(req, res) {
    // const papers = await Content.find({ author: req.user.id });
    const papers = await Content.aggregate([
      { $match: { author: ObjectId(req.user.id) } },
      {
        $lookup: {
          from: "results",
          localField: "_id",
          foreignField: "paper",
          as: "results",
        },
      },
      { $unwind: { path: "$results", preserveNullAndEmptyArrays: true } },
      {
        $group: {
          _id: "$_id",
          average: { $avg: "$results.percentage" },
          doc: { $first: "$$ROOT" },
        },
      },
      { $replaceRoot: { newRoot: "$doc" } },
    ]);
    res.json(papers);
  }

  static async setOption(req, res) {
    const option = req.body.option === 'autoMark' ? 'autoMark' : 'postOnNext';
    const doc = await Content.findByIdAndUpdate(req.params.pid, { $set: { [option]: ![option] } }, { new: true});
    res.json(doc[option]);
  }

  static async getAllMyNotes(req, res) {
    const notes = await Notes.find({ teacher: req.user.id });
    res.json(notes);
  }

  static async uploadVideo(req, res) {
    const video = new Videos(req.body);
    await video.save();

    res.json(video);
  }

  static async getVideos(req, res) {
    const videos = await Videos.find({ teacher: req.user.id });
    res.json(videos);
  }
}

export default TeachersController;
