import mongoose from "mongoose";
const Proffer = mongoose.model("Proffer");

class ProffersController {
  static async addSubmission(req, res) {
    const sub = new Proffer(req.body);
    sub.save();

    res.json(true);
  }

  static async getSubmission(req, res) {
    const doc = await Proffer.findOne({ student: req.user.id, assignment: req.params.assignmentId });
    
    res.json(doc);
  }
  
  static async updateSubmission(req, res) {
    const doc = await Proffer.findOne({ _id: req.params.id });
    Object.entries(req.body).forEach(([key, value]) => {
      doc[key] = value;
    });
    await doc.save();

    res.json(doc);
  }
  
  static async getTeacherSubmissions(req, res) {
    const profs = await Proffer.find({ assignment: req.params.id}).populate("student", "name");
    res.json(profs);
  }

  static async getStudentSubmissions(req, res) {
    const profs = await Proffer.find({ student: req.user.id});
    res.json(profs);
  }
  
  static async getClasses(req, res) {
    const classes = await Proffer.aggregate([
      { $match: { student: req.user.id } },
      { $lookup: { from: 'assignments', localField: 'assignment', foreignField: '_id', as: 'assignment' } },
      { $unwind: '$assignment.class'},
      { $project: { _id: 1, 'assignment.class': 1 } },
      { $group: { _id: '$assignment.class', count: { $sum: 1}}}
    ]);

    res.json(classes);
  }
}

export default ProffersController;