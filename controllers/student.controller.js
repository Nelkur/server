import fs from "fs";
import omit from "lodash.omit";
import mongoose from "mongoose";
const Student = mongoose.model("Student");
const Result = mongoose.model("Result");
import { LoginValidationSchema } from "../models/Students.js";
import { ResultValidationSchema } from "../models/Results.js";

class StudentsController {
  static async getAll(req, res) {
    const students = await Student.find();
    res.json(students);
  }

  static async login(req, res) {
    await LoginValidationSchema.validateAsync(req.body);

    const student = await Student.findOne({ code: req.body.code });
    if (!student) throw { status: 400, message: "Invalid student code!" };

    const token = student.generateAuthToken();
    res.json(token);
  }

  static async getCurrentStudent(req, res) {
    const student = await Student.find(
      { _id: req.user.id },
      { salt: 0, password: 0 }
    ).populate("school", "name");
    res.json(student);
  }

  static async updateProfile(req, res) {
    const doc = await Student.findOne({ _id: req.user.id });
    Object.entries(req.body).forEach(([key, value]) => {
      doc[key] = value;
    });
    await doc.save();

    res.json(doc);
  }

  static async uploadPhoto(req, res) {
    const { currentPhoto } = req.body;
    if (currentPhoto !== "undefined") {
      fs.unlink(`./public/uploads/users/${currentPhoto}`, (err) => {
        if (err) {
          throw err;
        }
      });
    }
    const doc = await Student.findOne({ _id: req.user.id });
    doc.photo = req.body.photo;
    await doc.save();

    res.json(omit(doc, ["salt", "password"]));
  }

  static async postResult(req, res) {
    await ResultValidationSchema.validateAsync(req.body);

    const result = new Result(req.body);
    await result.save();
    res.json({id: result._id});
  }

  static async getResults(req, res) {
    const results = await Result.find({ user: req.user.id }).populate("paper");
    res.json(results);
  }
}

export default StudentsController;
