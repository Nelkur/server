import fs from "fs";
import omit from "lodash.omit";
import mongoose from "mongoose";
const Individual = mongoose.model("Individual");
const Result = mongoose.model("Result");
const Grading = mongoose.model("Grading");
import {
  IndividualAccountValidationSchema,
  IndividualLoginValidationSchema,
} from "../models/Individuals.js";
import { ResultValidationSchema } from "../models/Results.js";
import { hash } from "../utils/hashes.js";

class IndividualController {
  static async getAll(req, res) {
    const all = await Individual.find();
    res.json(all);
  }

  static async register(req, res) {
    await IndividualAccountValidationSchema.validateAsync(req.body);

    let user = await Individual.findOne({ email: req.body.email });
    if (user) throw { status: 400, message: "That email is already in use" };

    user = new Individual(req.body);
    await user.save();

    const token = user.generateAuthToken();
    res
      .header("x-auth-token", token)
      .header("access-control-expose-headers", "x-auth-token")
      .json(omit(user, ["salt", "password"]));
  }

  static async login(req, res) {
    await IndividualLoginValidationSchema.validateAsync(req.body);

    const user = await Individual.findOne({ email: req.body.email });
    if (!user) throw { status: 400, message: "Invalid email or password" };

    const confirmPassword = await hash(req.body.password, user.salt);
    if (confirmPassword !== user.password)
      throw { status: 400, message: "Invalid email or password" };

    const token = user.generateAuthToken();
    res.json(token);
  }

  static async getCurrentUser(req, res) {
    const user = await Individual.findOne(
      { _id: req.user.id },
      { salt: 0, password: 0 }
    );
    res.json(user);
  }

  static async updateProfile(req, res) {
    const doc = await Individual.findOne({ _id: req.user.id });
    Object.entries(req.body).forEach(([key, value]) => {
      doc[key] = value;
    });
    await doc.save();

    res.json(doc);
  }

  static async uploadPhoto(req, res) {
    const { currentPhoto } = req.body;
    if (currentPhoto !== "undefined") {
      fs.unlink(`./public/uploads/users/${currentPhoto}`, (err) => {
        if (err) {
          throw err;
        }
      });
    }
    const doc = await Individual.findOne({ _id: req.user.id });
    doc.photo = req.body.photo;
    await doc.save();

    res.json(omit(doc, ["salt", "password"]));
  }

  static async postResult(req, res) {
    await ResultValidationSchema.validateAsync(req.body);

    const result = new Result(req.body);
    await result.save();
    res.json(result);
  }

  static async getResults(req, res) {
    const results = await Result.find({ user: req.user.id }).populate("paper");
    res.json(results);
  }

  static async setGrading(req, res) {
    const grading = await Grading.update(
      { _id: req.user.id },
      { $set: { grading: req.body.grading } },
      { upsert: true }
    );
    res.json(grading);
  }
}

export default IndividualController;
