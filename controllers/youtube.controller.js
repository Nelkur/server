import fs from "fs";
import multer from "multer";
import googleapi from "googleapis";
const { google } = googleapi;
import initializeYoutube from "../init/youtube.js";
let youtube;

initializeYoutube(getAuth);

function getAuth(oauth2Client) {
  // initialize the Youtube API library
  youtube = google.youtube({ version: "v3", auth: oauth2Client });
  // console.log("Oauth 2 client controller obj");
  // console.log(oauth2Client);
}

const mimetypes = [
  { mimetype: "mp4", ext: "mp4" },
  { mimetype: "quicktime", ext: "mov" },
  { mimetype: "x-flv", ext: "flv" },
  { mimetype: "x-msvideo", ext: "avi" },
  { mimetype: "x-ms-wmv", ext: "wmv" },
];

const multerVideoOptions = {
  storage: multer.diskStorage({
    destination: "./public/uploads/videos",
    filename: function (req, file, cb) {
      const mimetype = file.mimetype.split("/")[1];
      const m = mimetypes.filter((f) => f.mimetype === mimetype);
      if (m.length) {
        const datetimestamp = Date.now();
        const fname = `${datetimestamp}.${m[0].ext}`;
        cb(null, fname);
      }
    },
  }),
  limits: { files: 1, fileSize: "128GB" },
  fileFilter: function (req, file, cb) {
    const mimetype = file.mimetype.split("/")[1];
    const f = mimetypes.filter((f) => f.mimetype === mimetype);
    if (!f.length) {
      return cb(
        new Error("We only allow word documents and pdfs to be uploaded.")
      );
    }
    cb(null, true);
  },
};

class YoutubeController {
  static upload() {
    return multer(multerVideoOptions).single("video");
  }

  static async getChannel(req, res) {
    const channel = await youtube.channels.list({
      part: "snippet,contentDetails,statistics",
      forUsername: "ESchooling",
    });
    res.json(channel.data);
  }

  static async getAllPlaylists(req, res) {
    const { channelId } = req.params;
    const playlists = await youtube.playlists.list({
      part: "snippet",
      channelId,
    });

    res.json(playlists);
  }

  static async createPlaylist(req, res) {
    const { name } = req.body;
    const playlist = await youtube.playlists.insert({
      part: "snippet,status",
      resource: {
        snippet: {
          title: name,
          description: `${name} videos.`,
        },
        status: {
          privacyStatus: "private",
        },
      },
    });

    res.json(playlist.data);
  }

  /* Get all videos in a playlist by id for each school */
  static async getPlaylist(req, res) {
    const { id, etag } = req.params;
    // Create custom HTTP headers for the request to enable use of eTags
    const headers = {};
    if (etag) {
      headers["If-None-Match"] = etag;
    }
    const playlist = await youtube.playlists.list({
      part: "id,snippet,status,player", // removed etag
      id,
      headers,
    });

    res.json(playlist.data);
  }

  static async updatePlaylist(req, res) {
    const { id, name } = req.body;
    const playlist = await youtube.playlists.update({
      part: "id,snippet",
      resource: {
        id,
        snippet: {
          title: name,
          description: `${name} videos.`,
        },
      },
    });

    res.json(playlist.data);
  }

  static async uploadVideo(req, res) {
    const { title, description, category, tags } = req.body;
    const video = await youtube.videos.insert({
      part: "id,snippet,status,statistics,player,contentDetails",
      resource: {
        snippet: {
          title,
          description,
          tags,
          categoryId: "27",
        },
        status: {
          privacyStatus: "unlisted",
        },
      },
      media: {
        body: fs.createReadStream(req.file.path),
      },
    });

    fs.unlinkSync(req.file.path);

    res.json(video.data);
  }

  static async getVideo(req, res) {
    const { id } = req.params;
    const video = await youtube.videos.list({
      part: "id,snippet,status,statistics,player,contentDetails",
      id,
    });
    res.json(video.data);
  }

  static async deleteVideo(req, res) {
    const { id } = req.params;
    const video = await youtube.videos.delete({
      part: "id",
      id,
    });

    res.json(video);
  }

  static async getPlaylistItems(req, res) {
    const { id } = req.params;
    const items = await youtube.playlistItems.list({
      part: "id,snippet",
      resource: {
        id,
      },
    });

    res.json(items);
  }

  static async uploadVideoToPlaylist(req, res) {
    const { playlistId, videoId } = req.body;
    const video = await youtube.playlistItems.insert({
      part: "snippet",
      resource: {
        snippet: {
          playlistId,
          resourceId: {
            kind: "youtube#video",
            videoId,
          },
        },
      },
    });

    res.json(video);
  }

  // If successful, this method returns an HTTP 204 response code (No Content).
  static async deleteVideoFromPlaylist(req, res) {
    const { id } = req.params;
    const content = await youtube.playlistItems.delete({
      part: "id",
      id,
    });

    res.json(content);
  }

  static async deletePlaylist(req, res) {
    const { id } = req.body;
    const video = await youtube.playlists.delete({
      part: "id",
      id,
    });

    res.json(video);
  }
}

export default YoutubeController;
