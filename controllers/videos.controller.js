import mongoose from "mongoose";
const Videos = mongoose.model("Videos");

class VideosController {
  // Public Videos
  static async getAll(req, res) {
    const videos = await Videos.find({ availability: "public" }).populate(
      "school",
      "name"
    );
    res.json(videos);
  }

  // For School Admins
  static async getBySchool(req, res) {
    const videos = await Videos.find({ school: req.params.schoolId }).populate(
      "teacher",
      "title name"
    );
    res.json(videos);
  }

  // For Students
  static async getStudentVideos(req, res) {
    const videos = await Videos.find({
      school: req.params.schoolId,
      class: req.params.cls,
    }).populate("teacher", "title name");

    res.json(videos);
  }
}

export default VideosController;
