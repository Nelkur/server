import mongoose from "mongoose";
const Assignment = mongoose.model("Assignment");

class AssignmentController {
  static async getAll(req, res) {
    const docs = await Assignment.find({ school: req.body.school });
    res.json(docs);
  }

  static async getOne(req, res) {
    const doc = await Assignment.findOne({ _id: req.params.id });
    res.json(doc);
  }

  static async getByTeacher(req, res) {
    const docs = await Assignment.find({ teacher: req.params.id });
    res.json(docs);
  }

  static async getStudentAssignments(req, res) {
    const docs = await Assignment.find({
      school: req.user.school,
      class: { $in: [`${req.user.class}`]}
    });
    res.json(docs);
  }

  static async updateAssignmentStatus(req, res) {
    await Assignment.findOneAndUpdate({ _id: req.params.id }, req.body, { new: true });
    res.json(true);
  }

  static async addNew(req, res) {
    req.body.class = JSON.parse(req.body.class);
    const doc = new Assignment(req.body);
    await doc.save();
    res.json(doc);
  }

  static async updateAssignment(req, res) {
    if (currentFile) {
      fs.unlink(`./public/uploads/assignments/${currentFile}`, (err) => {
        if (err) {
          throw err;
        }
      });
    }
    const doc = await Assignment.findOne({ _id: req.params.id });
    Object.entries(req.body).forEach(([key, value]) => {
      doc[key] = value;
    });
    await doc.save();

    res.json(doc);
  }

  static async deleteAssignment(req, res) {
    const doc = await Assignment.deleteOne({ _id: req.params.id });
    res.json(doc);
  }
}

export default AssignmentController;
