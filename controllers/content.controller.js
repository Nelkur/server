import mongoose from "mongoose";
const Content = mongoose.model("Content");

class ContentController {
  static async getAll(req, res) {
    const content = await Content.find({}, { paper: 0 })
      .populate("school", "name system")
      .populate("author", "name");
    res.json(content);
  }

  // When someone decides to either take exam OR view marking scheme of a paper
  static async getOne(req, res) {
    const paper = await Content.findOne({ _id: req.body.id })
      .populate("school", "name grading")
      .populate("author", "name");
    res.json(paper);
  }

  static async cleanUp(req, res, next) {
    // console.log("Data before cleanup");
    // console.log(req.body);
    delete req.body.diagrams;
    delete req.body.dIndexes;
    delete req.body.nFiles;
    delete req.body.nIndexes;
    next();
  }

  static async addPaper(req, res) {
    if (typeof req.body.paper === "string") {
      req.body.paper = JSON.parse(req.body.paper);
    }
    const paper = new Content(req.body);
    await paper.save();

    // res.json(paper);
    res.json(true);
  }
}

export default ContentController;
