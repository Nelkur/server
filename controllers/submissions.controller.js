import mongoose from "mongoose";
const Submission = mongoose.model("Submission");
const Result = mongoose.model("Result");
import bson from "bson";
const ObjectId = bson.ObjectId; 

class SubmissionsController {
  static async addNew(req, res) {
    const submission = new Submission(req.body);
    await submission.save();

    res.json(submission);
  }

  static async updateStatus(req, res) {
    const sub = await Submission.findOne({ _id: req.body.id });
    sub.status = req.body.status;
    await sub.save();

    res.json(sub);
  }

  static async updateSubmissionStatus(req, res) {
    const sub = await Submission.findOne({ _id: req.params.id });
    sub.status = 'marked';
    await sub.save();

    res.json(sub);
  }

  static async getAllSubmissions(req, res) {
    const subs = await Submission.find({paper: req.params.pid, teacher: req.params.tid});
    res.json(subs);
  }

  static async getSubmissions(req, res) {
    console.log(req.params.tid);
    // const subs = await Submission.aggregete([
    //   { $match: { teacher: req.params.tid } },
    //   { $group: { _id: '$paper', count: { $sum: 1 } } },
    //   { $lookup: { from: 'content', localField: '_id', foreignField: '_id', as: 'paper' } },
    //   { $replaceRoot: { newRoot: { $mergeObjects: [{ $arrayElemAt: ["$paper", 0] }, "$$ROOT"] } } },
    //   { $project: { count: 1, code: 1, subject: 1, class: 1, date: 1, paper: 1, questionType: 1, paper: 1, autoMark: 1, postOnNext: 1 } },
    //   { $sort: { date: 1 }}
    // ]);
    res.json([]);
  }

  static async getMarkingSubmissions(req, res) {
    const subs = await Submission.find({ teacher: req.user.id, paper: req.params.pid, status: 'pending' }).populate('student', 'name school');
    res.json(subs);
  }

  static async deleteSubmissions(req, res) {
    const doc = await Submission.deleteMany({ paper: req.body.pid, teacher: req.body.tid });
    res.json(doc);
  }

  static async getResultsInHolding(req, res) {
    const results = await Result.aggregate([
      { $match: { paper: req.params.pid, status: 'holding' } },
      { $group: { _id: '$paper', count: { $sum: 1 } } },
      { $sort: { date: 1}}
    ]);
    res.json(results);
  }

  static async updateResultStatus(req, res) {
    const doc = await Result.updateMany({ paper: req.body.pid, author: req.body.tid });
    res.json(doc);
  }
}

export default SubmissionsController;
