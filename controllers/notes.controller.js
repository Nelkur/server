import mongoose from "mongoose";
import fs from "fs";
const Notes = mongoose.model("Notes");

class NotesController {
  static async getAll(req, res) {
    const notes = await Notes.find({ availability: "public" });
    res.json(notes);
  }

  static async getOne(req, res) {
    const doc = await Notes.findOne({ _id: req.params.id });
    res.json(doc);
  }

  // For School Admins
  static async getBySchool(req, res) {
    const notes = await Notes.find({ school: req.params.schoolId }).populate(
      "teacher",
      "title name photo"
    );
    res.json(notes);
  }

  // For Students
  static async getStudentNotes(req, res) {
    const notes = await Notes.find({
      school: req.params.schoolId,
      class: req.params.cls,
    }).populate("teacher", "title name photo");

    res.json(notes);
  }

  static async addNew(req, res) {
    const doc = new Notes(req.body);
    await doc.save();

    res.json(doc);
  }

  static async deleteNotes(req, res) {
    const doc = await Notes.findOne({ _id: req.params.id });
    console.log(doc);
    if (doc) {
      fs.unlink(`./public/uploads/notes/${doc.file}`, (err) => {
        if (err) {
          throw err;
        }
      });
      const d = await Notes.deleteOne({ _id: req.params.id });
      res.json(d);
    }
    throw { status: 401, message: "No such file exists." };
  }

  static async updateNotes(req, res) {
    const doc = await Notes.findOne({ _id: req.body.id });
    // If req.body.file then we have to replace the notes, so grab doc.file path & delete
    if (doc.teacher === req.user.id) {
      Object.entries(req.body).forEach(([key, value]) => {
        doc[key] = value;
      });
      await doc.save();

      res.json(doc);
    }
    throw { status: 403, message: "You are not the owner of this record." };
  }
}

export default NotesController;
