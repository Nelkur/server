import multer from "multer";
import jimp from "jimp";
import { v4 as uuidv4 } from "uuid";

const files = [
  { mimetype: "msword", ext: "doc" },
  {
    mimetype: "vnd.openxmlformats-officedocument.wordprocessingml.document",
    ext: "docx",
  },
  { mimetype: "pdf", ext: "pdf" },
];

let diagramCounter = 0;
let namingCounter = 0;
let namingLength = 0;

const multerImageOptions = {
  storage: multer.memoryStorage(),
  fileFilter(req, file, next) {
    const isPhoto = file.mimetype.startsWith("image/");
    return isPhoto
      ? next(null, true)
      : next({ message: "That file type is not allowed." }, false);
  },
};

const multerFileOptions = {
  storage: multer.diskStorage({
    destination: "./public/uploads/notes",
    filename: function (req, file, cb) {
      const mimetype = file.mimetype.split("/")[1];
      const m = files.filter((f) => f.mimetype === mimetype);
      if (m.length) {
        const datetimestamp = Date.now();
        const fname = `${datetimestamp}.${m[0].ext}`;
        req.body.file = fname;
        cb(null, fname);
      }
    },
  }),
  limits: { files: 1, fileSize: 3 * 1024 * 1024 },
  fileFilter: function (req, file, cb) {
    const mimetype = file.mimetype.split("/")[1];
    const f = files.filter((f) => f.mimetype === mimetype);
    if (!f.length) {
      return cb(
        new Error("We only allow word documents and pdfs to be uploaded.")
      );
    }
    cb(null, true);
  },
};

const multerAssignmentOptions = {
  storage: multer.diskStorage({
    destination: "./public/uploads/assignments",
    filename: function (req, file, cb) {
      const mimetype = file.mimetype.split("/")[1];
      if (mimetype === "pdf") {
        const datetimestamp = Date.now();
        const fname = `${datetimestamp}.pdf`;
        req.body.file = fname;
        cb(null, fname);
      }
    },
  }),
  limits: { files: 1, fileSize: 3 * 1024 * 1024 },
  fileFilter: function (req, file, cb) {
    const mimetype = file.mimetype.split("/")[1];
    if (mimetype !== "pdf") {
      return cb(new Error("We only allow pdf files to be uploaded."));
    }
    cb(null, true);
  },
};

const submitAssignmentOptions = {
  storage: multer.diskStorage({
    destination: "./public/uploads/assignments/submissions",
    filename: function (req, file, cb) {
      const ext = file.mimetype.split("/")[1];
      const datetimestamp = Date.now();
      const fname = `${datetimestamp}.${ext}`;
      if (req.body.files && req.body.files.length) {
        req.body.files.push(fname);
      } else {
        req.body.files = [fname];
      }
      cb(null, fname);
    },
  }),
  limits: { files: 8, fileSize: 3 * 1024 * 1024 },
  fileFilter: function (req, file, cb) {
    const mimetype = file.mimetype;
    if (mimetype.split('/')[1] !== 'pdf' && !mimetype.startsWith('image/')) {
      return cb(new Error("We only allow pdf files and photos to be uploaded."));
    }
    cb(null, true);
  },
};

const setPaperOptions = {
  storage: multer.memoryStorage(),
  async fileFilter(req, file, next) {
    console.log('---- Files ----');
    console.log(req.files);
    if (!Array.isArray(req.body.paper)) {
      console.log('Not array, converting...');
      req.body.paper = JSON.parse(req.body.paper);
    }
    const isPhoto = file.mimetype.startsWith("image/");
    if (isPhoto) {
      if (file.fieldname === 'diagrams') {
        console.log('*********** DIAGRAM *********** - ' + diagramCounter);
        diagramCounter++;
        const extension = file.mimetype.split("/")[1];
        const paperIndex = req.body.dIndexes[diagramCounter - 1];
        const fileName = `${uuidv4()}.${extension}`;
        console.log('File buffer');
        console.log(req.files.diagrams[paperIndex].buffer);

        // Now resize
        const photo = await jimp.read(req.files.diagrams[paperIndex].buffer);
        await photo.resize(800, jimp.AUTO);
        // Write to folder
        await photo.write(`./public/uploads/papers/diagrams/${fileName}`);
        req.body.paper[paperIndex].file = fileName;
      } else if (file.filedname === 'nFiles') {
        console.log('******** NAMING ********');
        // console.log('File buffer');
        const namingObj = req.body.nIndexes[namingCounter];
        let paperIndex;
        let namingIndex = 0;
        Object.entries(namingObj).forEach(([key, value]) => {
          paperIndex = key;
          namingLength = value.length;
        });

        const fileName = `${uuidv4()}.${extension}`;
        req.body.paper[paperIndex].nItems[namingIndex].image = fileName;
        // Now resize
        const photo = await jimp.read(req.file.buffer);
        await photo.resize(110, jimp.AUTO);
        // Write to folder
        await photo.write(`./public/uploads/papers/naming/${fileName}`);
        namingIndex++;
        if (namingIndex + 1 > namingLength) {
          namingIndex = 0;
          namingCounter++;
        }
      }
      return next(null, true);
    } else {
      return next({ message: "That file type is not allowed." }, false);
    }
  },
}

class UploadController {
  static upload() {
    return multer(multerImageOptions).single("photo");
  }

  static uploadFile() {
    return multer(multerFileOptions).single("file");
  }

  static uploadPaperFiles() {
    return multer(setPaperOptions).fields([{ name: 'diagrams' }, { name: 'nFiles' }]);
  }

  static uploadAssignment() {
    return multer(multerAssignmentOptions).single("file");
  }

  static submitAssignment() {
    return multer(submitAssignmentOptions).array("files");
  }

  static async resize(req, res, next) {
    if (!req.file) return next();
    const extension = req.file.mimetype.split("/")[1];
    req.body.photo = `${uuidv4()}.${extension}`;
    // Now resize
    console.log('File resize buffer');
    console.log(req.file.buffer);
    const photo = await jimp.read(req.file.buffer);
    await photo.resize(110, 110);
    // Write to folder
    await photo.write(`./public/uploads/users/${req.body.photo}`);
    next();
  }

  static async resizeDiagram(req, res, next) {
    // Check if file exists, else
    if (!req.file) return next();
    const extension = req.file.mimetype.split("/")[1];
    req.body.photo = `${uuidv4()}.${extension}`;
    // Now resize
    const photo = await jimp.read(req.file.buffer);
    await photo.resize(800, jimp.AUTO);
    // Write to folder
    await photo.write(`./public/uploads/diagrams/${req.body.photo}`);
    next();
  }
}

export default UploadController;
