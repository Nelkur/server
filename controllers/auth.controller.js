import mongoose from "mongoose";
const Institution = mongoose.model("Institution");
const Teacher = mongoose.model("Teacher");
const Individual = mongoose.model("Individual");
import Jwt from "../utils/tokens.js";
import crypto from "crypto";
import omit from "lodash.omit";
import Mail from "../handlers/mail.js";

class AuthController {
  static isLoggedIn(req, res, next) {
    const token = req.header("x-auth-token");
    if (!token)
      // Unauthorized
      throw { status: 401, message: "Access denied! No token provided!" };

    try {
      const decoded = Jwt.validateToken(token);
      req.user = decoded;
      next();
    } catch (ex) {
      throw { status: 400, message: "Invalid token!" };
    }
  }

  static isAdmin(req, res, next) {
    if (!req.user.isAdmin) throw { status: 403, message: "Access denied." }; // Forbidden
    next();
  }

  static isTeacher(req, res, next) {
    if (!req.user.isTeacher) throw { status: 403, message: "Access denied." }; // Forbidden
    next();
  }

  static isClassAdmin(req, res, next) {
    if (!req.user.isClassAdmin)
      throw { status: 403, message: "Access denied." }; // Forbidden
    next();
  }

  static async isStudent(req, res, next) {
    if (!req.user.isStudent) throw { status: 403, message: "Access denied." };
    next();
  }

  static async isPersonalAccount(req, res, next) {
    if (!req.user.type === "individual")
      throw { status: 403, message: "Access denied." };
    next();
  }

  async getModel(type) {
    try {
      return new Promise((resolve, reject) => {
        if (type === "institution") {
          resolve(Institution);
        } else if (type === "teacher") {
          resolve(Teacher);
        } else if (type === "individual") {
          resolve(Individual);
        } else {
          reject("Invalid type!");
        }
      });
    } catch (err) {
      throw err;
    }
  }

  static async forgotPassword(req, res) {
    // 1. Confirm that user exists
    let user;
    const type = req.body.type;
    if (type === "institution") {
      user = await Institution.findOne({ email: req.body.email });
    } else if (type === "teacher") {
      user = await Teacher.findOne({ email: req.body.email });
    } else {
      user = await Individual.findOne({ email: req.body.email });
    }
    if (!user)
      throw { status: 400, message: `No account exists with that email.` };
    // 2. Set reset token and expiry on their account
    user.resetPasswordToken = crypto.randomBytes(20).toString("hex");
    user.resetPasswordExpires = Date.now() + 3600000;
    await user.save();
    // 3. Send them an email with the token in form of a link
    const resetURL = `https://eschooling-dev.herokuapp.com/account/reset/${req.body.type}/${user.resetPasswordToken}`;
    await Mail.send({
      user,
      filename: "password-reset",
      subject: "Password Reset",
      resetURL,
    });
    // 4. Return result
    res.json({ message: resetURL });
  }

  // Confirm that the user's token is valid when they access this link from their email
  static async resetPassword(req, res) {
    // const model = await this.getModel(req.params.type);
    let user;
    const type = req.params.type;
    if (type === "institution") {
      user = await Institution.findOne(
        {
          resetPasswordToken: req.params.token,
          resetPasswordExpires: { $gt: Date.now() },
        },
        { salt: 0, password: 0 }
      );
    } else if (type === "teacher") {
      user = await Teacher.findOne(
        {
          resetPasswordToken: req.params.token,
          resetPasswordExpires: { $gt: Date.now() },
        },
        { salt: 0, password: 0 }
      );
    } else {
      user = await Individual.findOne(
        {
          resetPasswordToken: req.params.token,
          resetPasswordExpires: { $gt: Date.now() },
        },
        { salt: 0, password: 0 }
      );
    }
    if (!user)
      throw { status: 400, message: "Password reset invalid or expired." };
    res.json(true);
  }

  static confirmPasswords(req, res, next) {
    if (req.body.password === req.body.confirmPassword) {
      next();
      return;
    }
    throw { status: 400, message: "Passwords do not match." };
  }

  static async updatePassword(req, res) {
    let user;
    const type = req.params.type;
    if (type === "institution") {
      user = await Institution.findOne(
        {
          resetPasswordToken: req.params.token,
          resetPasswordExpires: { $gt: Date.now() },
        },
        { salt: 0, password: 0 }
      );
    } else if (type === "teacher") {
      user = await Teacher.findOne(
        {
          resetPasswordToken: req.params.token,
          resetPasswordExpires: { $gt: Date.now() },
        },
        { salt: 0, password: 0 }
      );
    } else {
      user = await Individual.findOne(
        {
          resetPasswordToken: req.params.token,
          resetPasswordExpires: { $gt: Date.now() },
        },
        { salt: 0, password: 0 }
      );
    }

    if (!user)
      throw { status: 400, message: "Password reset invalid or expired." };

    user.password = req.body.password;
    user.resetPasswordToken = undefined;
    user.resetPasswordExpires = undefined;
    await user.save();

    const token = user.generateAuthToken();
    res
      .header("x-auth-token", token)
      .header("access-control-expose-headers", "x-auth-token")
      .json(omit(user, ["salt", "password"]));
  }
}

export default AuthController;
