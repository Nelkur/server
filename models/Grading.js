import mongoose from "mongoose";
const { Schema } = mongoose;
import Joi from "@hapi/joi";

const GradesSchema = new Schema(
  {
    system: {
      type: String,
      required: true,
    },
    grade: {
      type: String,
      trim: true,
      maxlength: 2,
      required: true,
    },
    from: {
      type: Number,
      max: 100,
      required: true,
    },
    to: {
      type: Number,
      max: 100,
      required: true,
    },
  },
  { collection: "grading" }
);

export const GradeValidationSchema = Joi.object({
  system: Joi.string().required(),
  grade: Joi.string().max(2).required(),
  from: Joi.number().integer().required(),
  to: Joi.number().integer().max(100).required(),
});

export default mongoose.model("Grading", GradesSchema);
