import mongoose from "mongoose";
const { Schema } = mongoose;
import Joi from "@hapi/joi";
import { generateSalt, hash } from "../utils/hashes.js";
import Jwt from "../utils/tokens.js";

const TeachersSchema = new Schema({
  title: String,
  name: {
    type: String,
    trim: true,
  },
  subjects: [String],
  email: {
    type: String,
    unique: true,
    required: true,
  },
  salt: String,
  password: String,
  school: {
    type: Schema.Types.ObjectId,
    ref: "Institution",
    required: true,
  },
  role: {
    type: String,
    required: true,
  },
  class: Number,
  photo: String,
  added: { type: Date, default: Date.now },
  resetPasswordToken: String,
  resetPasswordExpires: Date,
});

TeachersSchema.methods.generateAuthToken = function () {
  return Jwt.generateToken({
    id: this._id,
    type: "teacher",
    name: this.name,
    email: this.email,
    isTeacher: true,
    school: this.school,
    isClassAdmin: this.role === "class admin" ? true : false,
  });
};

TeachersSchema.pre("save", async function (next) {
  if (!this.isModified("password")) return next();
  this.salt = await generateSalt();
  this.password = await hash(this.password, this.salt);
  next();
});

export const TeacherValidationSchema = Joi.object({
  title: Joi.string().min(2).max(4).required(),
  name: Joi.string().required(),
  subjects: Joi.array().items(Joi.string()).required(),
  email: Joi.string().email().required(),
  password: Joi.string().min(8).max(24).required(),
});

export const LoginValidationSchema = Joi.object({
  email: Joi.string().email().required(),
  password: Joi.string().min(8).max(24).required(),
});

export default mongoose.model("Teacher", TeachersSchema);
