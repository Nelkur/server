import mongoose from "mongoose";
const { Schema } = mongoose;
import Duration from "../utils/videoDuration.js";

const VideosSchema = new Schema(
  {
    subject: { type: String, required: true },
    class: { type: Number, required: true },
    topic: { type: String, required: true },
    title: { type: String, required: true },
    description: String,
    availability: { type: String, required: true },
    thumbnail: { type: String, required: true },
    duration: { type: String, required: true },
    embedHtml: { type: String, required: true },
    teacher: { type: Schema.Types.ObjectId, ref: "Teacher" },
    school: { type: Schema.Types.ObjectId, ref: "Institution" },
    date: { type: Date, default: Date.now },
  },
  { collection: "videos" }
);

// VideosSchema.pre("save", function (next) {
//   if (!this.isModified("duration")) return next();
//   this.duration = Duration(this.duration);
//   next();
// });

export default mongoose.model("Videos", VideosSchema);
