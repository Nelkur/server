import mongoose from "mongoose";
const { Schema } = mongoose;
import { DurationSchema } from "./Subjects.js";
import ExamCode from "../utils/paperCode.js";

const ChoicesSchema = new Schema(
  {
    option: { type: String, maxlength: 1 },
    answer: { type: String },
  },
  { _id: false }
);

const PaperSchema = new Schema(
  {
    // number: { type: Number, unique: true, required: true },
    image: String,
    passage: String,
    question: { type: String, required: true },
    questionType: String,
    choices: [ChoicesSchema],
    marks: { type: Number, max: 100 },
    isEssay: Boolean,
    wordLimit: Number,
    correctAnswer: String,
    explanation: String,
    ddCategories: [String],
    ddItems: [String],
    nItems: [{ image: String, name: String }],
    fibType: String,
    fibItems: [{ word: String, blank: String }],
    dashItems: [String],
    newSection: { type: Boolean, default: false },
    sectionInstructions: String,
  },
  { _id: true }
);

const ContentSchema = new Schema(
  {
    subject: { type: String, required: true },
    class: { type: Number, required: true },
    type: { type: String, required: true },
    title: { type: String, required: true },
    availability: { type: String, required: true },
    scheduledFor: Date,
    duration: { type: DurationSchema },
    author: { type: Schema.Types.ObjectId, ref: "Teacher" },
    school: { type: Schema.Types.ObjectId, ref: "Institution" },
    code: String,
    scope: String,
    topicsCovered: [String],
    questionType: { type: String, required: true },
    set: { type: String, default: "none" },
    paper: [PaperSchema],
    autoMark: { type: Boolean, default: false },
    postOnNext: { type: Boolean, default: false },
    date: { type: Date, default: Date.now },
  },
  { collection: "content" }
);

ContentSchema.pre("save", function (next) {
  // if (!this.isModified("code") || this.code) {
  //   next();
  //   return;
  // }
  // const random = randomize(1, 9999);
  // const code = `${this.code}-${random}`;
  // this.code = code.toUpperCase();
  this.code = ExamCode.generateCode(this.subject, this.type);
  next();
});

export default mongoose.model("Content", ContentSchema);
