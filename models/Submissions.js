import mongoose from "mongoose";
const { Schema } = mongoose;

const AnswerSchema = new Schema(
  {
    question: Number,
    answer: Schema.Types.Mixed,
    correct: Schema.Types.Mixed,
    awardedMarks: Number,
    totalMarks: Number
  },
  { _id: false }
);

const SubmissionSchema = new Schema({
  student: { type: Schema.Types.ObjectId, ref: "Student" },
  paper: { type: Schema.Types.ObjectId, ref: "Content" },
  answers: [AnswerSchema],
  // teacher: { type: Schema.Types.ObjectId, ref: "Teacher" },
  timeTaken: { type: String, required: true },
  status: { type: String, required: true},
  date: { type: Date, default: Date.now },
});

export default mongoose.model("Submission", SubmissionSchema);
