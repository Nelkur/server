import mongoose from "mongoose";
const { Schema } = mongoose;

const ProfferSchema = new Schema({
  student: { type: Schema.Types.ObjectId, ref: "Student" },
  assignment: { type: Schema.Types.ObjectId, ref: "Assignment" },
  type: { type: String, required: true},
  marks: Number,
  comments: String,
  penalty: Number,
  files: [String],
  submissionStatus: { type: String, required: true},
  status: { type: String, required: true },
  date: { type: Date, default: Date.now },
});

export default mongoose.model("Proffer", ProfferSchema);
