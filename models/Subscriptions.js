import mongoose from "mongoose";
const { Schema } = mongoose;
import Joi from "@hapi/joi";

const SubscriptionsSchema = new Schema({
  accountType: {
    type: String, // Personal / School
    trim: true,
    required: true,
  },
  accountId: {
    type: Schema.Types.ObjectId,
    required: true,
    unique: true
  },
  subscriptionType: {
    type: String,
    required: true,
    trim: true,
  },
  teachers: Number,
  students: Number,
  paymentId: {
    type: Schema.Types.ObjectId,
    ref: "Payment",
    required: true,
  },
  paymentMethod: {
    type: String,
    required: true,
  },
  amount: {
    type: Number,
    required: true,
  },
  description: { type: String, required: true },
  status: { type: String, required: true },
  expiry: { type: Date, required: true },
  created: { type: Date, default: Date.now },
  updated: { type: Date, default: Date.now },
});

export const SubscriptionValidationSchema = Joi.object({
  accountType: Joi.string().required(),
  accountId: Joi.string().alphanum().min(24).max(24).required(),
  subscriptionType: Joi.string().required(),
  transactionCode: Joi.string().alphanum().required(),
  amount: Joi.number().integer().required(),
});

export default mongoose.model("Subscription", SubscriptionsSchema);
