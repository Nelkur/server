import mongoose from "mongoose";
const { Schema } = mongoose;
import Joi from "@hapi/joi";
import { generateSalt, hash } from "../utils/hashes.js";
import Jwt from "../utils/tokens.js";
import { Slugify, SlugRegex } from "../utils/slugs.js";

const GradesSchema = new Schema(
  {
    grade: {
      type: String,
      trim: true,
      maxlength: 2,
      required: true,
    },
    from: {
      type: Number,
      max: 100,
      required: true,
    },
    to: {
      type: Number,
      max: 100,
      required: true,
    },
  },
  { _id: false }
);

const InstitutionSchema = new Schema({
  type: {
    type: String, // School / Publisher / ?
    required: true,
  },
  system: String, // School system i.e. GCSE / Regular
  name: {
    type: String,
    required: true,
    trim: true,
  },
  slug: String,
  phone: {
    type: String,
    required: false,
    maxlength: 13,
    minlength: 10,
  },
  address: String,
  email: {
    type: String,
    required: true,
    unique: true,
  },
  salt: String,
  password: {
    type: String,
    required: true,
  },
  adminName: {
    type: String,
    required: true,
    trim: true,
  },
  playlistId: { type: String, required: true },
  photo: String,
  grading: [GradesSchema], // If admin modifies grades, set the custom one here
  created: { type: Date, default: Date.now },
  resetPasswordToken: String,
  resetPasswordExpires: Date,
});

InstitutionSchema.methods.generateAuthToken = function () {
  return Jwt.generateToken({
    id: this._id,
    slug: this.slug,
    type: "institution",
    institutionName: this.name,
    email: this.email,
    phone: this.phone,
    isAdmin: true,
    name: this.adminName,
  });
};

InstitutionSchema.pre("save", async function (next) {
  if (!this.isModified("password")) return next();
  this.salt = await generateSalt();
  this.password = await hash(this.password, this.salt);

  if (!this.isModified("name")) return next();
  this.slug = Slugify(this.name);
  const slugRegEx = SlugRegex(this.slug);
  const matchedRecords = await this.constructor.find({
    slug: { $regex: slugRegEx },
  });
  if (matchedRecords.length) {
    this.slug = `${this.slug}-${matchedRecords.length + 1}`;
  }

  next();
});

export const InstitutionValidationSchema = Joi.object({
  type: Joi.string().required(),
  system: Joi.string(),
  name: Joi.string().required(),
  phone: Joi.string().alphanum().min(10).max(13).required(),
  address: Joi.string().required(),
  email: Joi.string().email().required(),
  password: Joi.string().min(8).max(24).required(),
  password: Joi.string().min(8).max(24).required(),
  confirmPassword: Joi.string().min(8).max(24).required(),
  adminName: Joi.string().min(2).max(20).required(),
  playlistId: Joi.string().required(),
});

export const LoginValidationSchema = Joi.object({
  email: Joi.string().email().required(),
  password: Joi.string().min(8).max(24).required(),
});

export const GradesValidationSchema = Joi.array().items(
  Joi.object({
    grade: Joi.string().max(2).required(),
    from: Joi.number().integer().max(100).required(),
    to: Joi.number().integer().max(100).required(),
  })
);

export default mongoose.model("Institution", InstitutionSchema);
