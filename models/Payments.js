import mongoose from "mongoose";
const { Schema } = mongoose;

const CallbackMetadataSchema = new Schema(
  {
    amount: {
      type: Number,
      required: true,
    },
    mpesaReceiptNumber: {
      type: String,
      required: true,
      unique: true,
    },
    balance: Number,
    transactionDate: {
      type: Date,
      required: true,
    },
    phoneNumber: {
      type: Number,
      required: true,
    },
  },
  { _id: false }
);

const PaymentSchema = new Schema({
  type: {
    type: String,
    required: true,
  },
  accountType: {
    type: String,
    trim: true,
    required: true,
  },
  accountId: {
    type: Schema.Types.ObjectId,
    required: true,
  },
  subscriptionType: {
    type: String,
    required: true,
  },
  teachers: Number,
  students: Number,
  merchantRequestId: {
    type: String,
    required: true,
  },
  checkoutRequestId: {
    type: String,
    required: true,
  },
  resultCode: {
    type: Number,
    required: true,
  },
  resultDesc: {
    type: String,
    required: true,
  },
  callbackMetadata: {
    type: CallbackMetadataSchema,
    required: true,
  },
});

export default mongoose.model("Payment", PaymentSchema);
