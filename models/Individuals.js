import mongoose from "mongoose";
const { Schema } = mongoose;
import Joi from "@hapi/joi";
import { generateSalt, hash } from "../utils/hashes.js";
import Jwt from "../utils/tokens.js";

const IndividualSchema = new Schema({
  firstname: {
    type: String,
    trim: true,
    required: true,
  },
  lastname: {
    type: String,
    trim: true,
    required: true,
  },
  phone: {
    type: String,
    required: false,
    maxlength: 13,
    minlength: 10,
  },
  email: {
    type: String,
    unique: true,
    required: true,
  },
  salt: String,
  password: {
    type: String,
    required: true,
  },
  photo: String,
  added: { type: Date, default: Date.now },
  resetPasswordToken: String,
  resetPasswordExpires: Date,
});

IndividualSchema.methods.generateAuthToken = function () {
  return Jwt.generateToken({
    id: this._id,
    type: "individual",
    name: `${this.firstname} ${this.lastname}`,
    email: this.email,
    phone: this.phone,
  });
};

IndividualSchema.pre("save", async function (next) {
  if (!this.isModified("password")) return next();
  this.salt = await generateSalt();
  this.password = await hash(this.password, this.salt);
  next();
});

export const IndividualAccountValidationSchema = Joi.object({
  firstname: Joi.string().min(2).max(20).required(),
  lastname: Joi.string().min(2).max(20).required(),
  phone: Joi.string().alphanum().min(10).max(13),
  email: Joi.string().email().required(),
  password: Joi.string().min(8).max(24).required(),
  confirmPassword: Joi.string().min(8).max(24),
});

export const IndividualLoginValidationSchema = Joi.object({
  email: Joi.string().email().required(),
  password: Joi.string().min(8).max(24).required(),
});

export default mongoose.model("Individual", IndividualSchema);
