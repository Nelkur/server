import mongoose from "mongoose";
const { Schema } = mongoose;
import Joi from "@hapi/joi";

const AnswerSchema = new Schema(
  {
    question: { type: Number, requried: true},
    answer: Schema.Types.Mixed,
    correct: Schema.Types.Mixed,
    awardedMarks: Number,
    totalMarks: Number
  },
  { _id: false }
);

const ResultsSchema = new Schema({
  accountType: { type: String, required: true }, // Not sure this is necessary. See directly below
  user: { type: Schema.Types.ObjectId, required: true, refPath: "onModel" },
  onModel: { type: String, required: true, enum: ["Student", "Individual"] },
  paper: { type: Schema.Types.ObjectId, ref: "Content" },
  school: { type: Schema.Types.ObjectId, required: false },
  attempted: { type: String, required: true },
  scored: { type: String, required: true },
  percentage: { type: Number, max: 100, required: true },
  grade: { type: String, maxlength: 2, required: true },
  answers: [AnswerSchema],
  status: { type: String, default: 'holding'}, // Holding or Delivered
  date: { type: Date, default: Date.now },
});

export const ResultValidationSchema = Joi.object({
  accountType: Joi.string().required(),
  user: Joi.string().alphanum().min(24).max(24).required(),
  onModel: Joi.string().required(),
  paper: Joi.string().alphanum().min(24).max(24).required(),
  school: Joi.string(),
  attempted: Joi.string().required(),
  scored: Joi.string().required(),
  percentage: Joi.number().integer().max(100).required(),
  grade: Joi.string().max(2).required(),
});

export default mongoose.model("Result", ResultsSchema);
