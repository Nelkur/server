import mongoose from "mongoose";
const { Schema } = mongoose;
import Joi from "@hapi/joi";
import { Slugify, SlugRegex } from "../utils/slugs.js";

export const DurationSchema = new Schema(
  {
    hours: {
      type: Number,
      max: 2,
      default: 0,
    },
    minutes: {
      type: Number,
      max: 59,
      default: 0,
    },
  },
  { _id: false }
);

const SubjectSchema = new Schema({
  name: {
    type: String,
    lowercase: true,
    trim: true,
    required: true,
  },
  slug: String,
  schoolSystem: {
    type: String, // All, GCSE, Regular
    required: true,
  },
  examDuration: {
    type: DurationSchema,
    required: true,
  },
});

SubjectSchema.pre("save", async function (next) {
  if (!this.isModified("name")) {
    next();
    return;
  }
  this.slug = Slugify(this.name);
  const slugRegEx = SlugRegex(this.slug);
  const matchedRecords = await this.constructor.find({
    slug: { $regex: slugRegEx },
  });
  if (matchedRecords.length) {
    this.slug = `${this.slug}-${matchedRecords.length + 1}`;
  }
  next();
});

export const SubjectValidationSchema = Joi.object({
  name: Joi.string().lowercase().required(),
  schoolSystem: Joi.string().required(),
  examDuration: Joi.object({
    hours: Joi.number().integer().max(2).required(),
    minutes: Joi.number().integer().max(59).required(),
  }),
});

export default mongoose.model("Subject", SubjectSchema);
