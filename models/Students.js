import mongoose from "mongoose";
const { Schema } = mongoose;
import Joi from "@hapi/joi";
import Jwt from "../utils/tokens.js";
import randomize from "../utils/randomize.js";

const StudentSchema = new Schema({
  name: {
    type: String,
    required: true,
    trim: true,
  },
  class: {
    type: Number,
    min: 1,
    max: 8,
    required: true,
  },
  code: {
    type: String,
    unique: true,
    required: true,
  },
  photo: String,
  school: {
    type: Schema.Types.ObjectId,
    ref: "Institution",
    required: true,
  },
  added: { type: Date, default: Date.now },
  status: { type: String, default: "Active" },
  averageScore: { type: Number, default: 0 },
  resetPasswordToken: String,
  resetPasswordExpires: Date,
});

StudentSchema.methods.generateAuthToken = function () {
  return Jwt.generateToken({
    id: this._id,
    type: "student",
    name: this.name,
    isStudent: true,
    school: this.school,
    class: this.class,
  });
};

StudentSchema.pre("save", function (next) {
  if (!this.isModified("code")) {
    next();
    return;
  }
  const random = randomize(1, 9999);
  const code = `${this.code}-${random}`;
  this.code = code.toUpperCase();
});

export const StudentValidationSchema = Joi.object({
  name: Joi.string().min(2).max(20).required(),
  class: Joi.number().integer().min(1).max(8).required(),
  code: Joi.string().required(),
  school: Joi.string().alphanum().min(24).max(24).required(),
});

export const LoginValidationSchema = Joi.object({
  code: Joi.string().required(),
});

export default mongoose.model("Student", StudentSchema);
