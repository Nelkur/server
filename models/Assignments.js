import mongoose from "mongoose";
const { Schema } = mongoose;

const TimeAllowanceSchema = new Schema({
  hours: Number,
  minutes: Number,
}, { _id: false});

const AssignmentSchema = new Schema({
  subject: { type: String, required: true },
  class: [{ type: String, required: true }],
  title: { type: String, required: true },
  marks: { type: Number, required: true },
  instructions: { type: String, required: true },
  dueDate: { type: Date, required: true },
  lateSubmission: { type: Boolean, required: true },
  timeAllowance: { type: TimeAllowanceSchema },
  penalty: { type: Boolean, default: false },
  penaltyPercentage: { type: Boolean, default: false },
  forfeit: Number,
  file: { type: String, required: true },
  status: { type: String, default: "pending" },
  school: { type: Schema.Types.ObjectId, ref: "Institution" },
  teacher: { type: Schema.Types.ObjectId, ref: "Teacher" },
  date: { type: Date, default: Date.now },
});

export default mongoose.model("Assignment", AssignmentSchema);
