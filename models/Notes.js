import mongoose from "mongoose";
const { Schema } = mongoose;

const NotesSchema = new Schema(
  {
    subject: { type: String, required: true },
    class: { type: Number, required: true },
    topic: { type: String, required: true },
    availability: { type: String, required: true },
    description: String,
    file: String,
    teacher: { type: Schema.Types.ObjectId, ref: "Teacher" },
    school: { type: Schema.Types.ObjectId, ref: "Institution" },
    date: { type: Date, default: Date.now },
  },
  { collection: "notes" }
);

export default mongoose.model("Notes", NotesSchema);
