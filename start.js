import InitializeDB from "./init/db.js";
import app from "./app.js";

InitializeDB();
app.set("port", process.env.PORT || 3000);
const server = app.listen(app.get("port"), () => {
  // logger.info(`Express running → PORT ${server.address().port}`);
  console.log(`Express running → PORT ${server.address().port}`);
});
