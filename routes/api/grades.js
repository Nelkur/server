import express from "express";
import GradingController from "../../controllers/grading.controller.js";
import { catchErrors } from "../../handlers/errorHandler.js";
const GradingRouter = express.Router();

GradingRouter.route("/")
  .get(catchErrors(GradingController.getAll))
  .post(catchErrors(GradingController.addNew));

GradingRouter.route("/:system").get(catchErrors(GradingController.getOne));

export default GradingRouter;
