import express from "express";
import StudentsController from "../../controllers/student.controller.js";
import AuthController from "../../controllers/auth.controller.js";
import UploadController from "../../controllers/uploads.controller.js";
import { catchErrors } from "../../handlers/errorHandler.js";
const StudentsRouter = express.Router();

StudentsRouter.route("/").get(catchErrors(StudentsController.getAll));

StudentsRouter.route("/login").post(catchErrors(StudentsController.login));

StudentsRouter.route("/me")
  .get(
    [AuthController.isLoggedIn, AuthController.isStudent],
    catchErrors(StudentsController.getCurrentStudent)
  )
  .post(
    [AuthController.isLoggedIn, AuthController.isStudent],
    UploadController.upload(),
    catchErrors(UploadController.resize),
    catchErrors(StudentsController.uploadPhoto)
  )
  .put(
    [AuthController.isLoggedIn, AuthController.isStudent],
    catchErrors(StudentsController.updateProfile)
  );

StudentsRouter.route("/results")
  .get(
    [AuthController.isLoggedIn, AuthController.isStudent],
    catchErrors(StudentsController.getResults)
  )
  .post(
    [AuthController.isLoggedIn, AuthController.isStudent],
    catchErrors(StudentsController.postResult)
  );

export default StudentsRouter;
