import express from "express";
import SubmissionsController from "../../controllers/submissions.controller.js";
import AuthController from "../../controllers/auth.controller.js";
import { catchErrors } from "../../handlers/errorHandler.js";
const SubmissionsRouter = express.Router();

SubmissionsRouter.route("/")
  .post(
    [AuthController.isLoggedIn, AuthController.isStudent],
    catchErrors(SubmissionsController.addNew)
  )
  .put(
    [AuthController.isLoggedIn, AuthController.isStudent],
    catchErrors(SubmissionsController.updateStatus)
);

SubmissionsRouter.route('/post-result')
  .put([AuthController.isLoggedIn, AuthController.isTeacher], catchErrors(SubmissionsController.updateResultStatus))
  .delete([AuthController.isLoggedIn, AuthController.isTeacher], catchErrors(SubmissionsController.deleteSubmissions));

SubmissionsRouter.route('/:tid/:pid')
  .get([AuthController.isLoggedIn, AuthController.isTeacher], catchErrors(SubmissionsController.getAllSubmissions));
  
SubmissionsRouter.route('/status/:id').delete(
  [AuthController.isLoggedIn, AuthController.isStudent],
  catchErrors(SubmissionsController.updateSubmissionStatus
));

SubmissionsRouter.route("/teacher/:tid").get(
  [AuthController.isLoggedIn, AuthController.isTeacher],
  catchErrors(SubmissionsController.getSubmissions)
);

SubmissionsRouter.route('/marking/:pid').get(
  [AuthController.isLoggedIn, AuthController.isTeacher],
  catchErrors(SubmissionsController.getMarkingSubmissions)
);

SubmissionsRouter.route('/holding/:pid').get(
  [AuthController.isLoggedIn, AuthController.isTeacher],
  catchErrors(SubmissionsController.getResultsInHolding)
);

export default SubmissionsRouter;
