import express from "express";
import IndividualController from "../../controllers/individual.controller.js";
import AuthController from "../../controllers/auth.controller.js";
import UploadController from "../../controllers/uploads.controller.js";
import { catchErrors } from "../../handlers/errorHandler.js";
const IndividualRouter = express.Router();

IndividualRouter.route("/").get(catchErrors(IndividualController.getAll));

IndividualRouter.route("/register").post(
  catchErrors(IndividualController.register)
);

IndividualRouter.route("/login").post(catchErrors(IndividualController.login));

IndividualRouter.route("/me")
  .get(
    [AuthController.isLoggedIn, AuthController.isPersonalAccount],
    catchErrors(IndividualController.getCurrentUser)
  )
  .post(
    [AuthController.isLoggedIn, AuthController.isPersonalAccount],
    UploadController.upload(),
    catchErrors(UploadController.resize),
    catchErrors(IndividualController.uploadPhoto)
  )
  .put(
    [AuthController.isLoggedIn, AuthController.isPersonalAccount],
    catchErrors(IndividualController.updateProfile)
  );

IndividualRouter.route("/results")
  .get(
    [AuthController.isLoggedIn, AuthController.isPersonalAccount],
    catchErrors(IndividualController.getResults)
  )
  .post(
    [AuthController.isLoggedIn, AuthController.isPersonalAccount],
    catchErrors(IndividualController.postResults)
  );

export default IndividualRouter;
