import express from "express";
import AuthController from "../../controllers/auth.controller.js";
import { catchErrors } from "../../handlers/errorHandler.js";
const AuthRouter = express.Router();

AuthRouter.route("/forgot").post(catchErrors(AuthController.forgotPassword));

AuthRouter.route("/reset/:type/:token")
  .get(catchErrors(AuthController.resetPassword))
  .post(
    AuthController.confirmPasswords,
    catchErrors(AuthController.updatePassword)
  );

export default AuthRouter;
