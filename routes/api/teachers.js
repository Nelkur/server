import express from "express";
import TeachersController from "../../controllers/teachers.controller.js";
import AuthController from "../../controllers/auth.controller.js";
import UploadController from "../../controllers/uploads.controller.js";
import { catchErrors } from "../../handlers/errorHandler.js";
const TeachersRouter = express.Router();

TeachersRouter.route("/").get(catchErrors(TeachersController.getAll));

TeachersRouter.route("/login").post(catchErrors(TeachersController.login));

TeachersRouter.route("/me")
  .get(
    [AuthController.isLoggedIn, AuthController.isTeacher],
    catchErrors(TeachersController.getOne)
  )
  .post(
    [AuthController.isLoggedIn, AuthController.isTeacher],
    UploadController.upload(),
    catchErrors(UploadController.resize),
    catchErrors(TeachersController.uploadPhoto)
  )
  .put(
    [AuthController.isLoggedIn, AuthController.isTeacher],
    catchErrors(TeachersController.updateTeacher)
  );

TeachersRouter.route("/register/:id")
  .get(catchErrors(TeachersController.registerTeacherLanding))
  .post(catchErrors(TeachersController.registerTeacher));

TeachersRouter.route("/me/papers")
  .get(
    [AuthController.isLoggedIn, AuthController.isTeacher],
    catchErrors(TeachersController.getAllMyPapers)
  )
  .post(
    [AuthController.isLoggedIn, AuthController.isTeacher],
    catchErrors(TeachersController.setPaper)
);

TeachersRouter.route("/me/students")
  .get(
    [
      AuthController.isLoggedIn,
      AuthController.isTeacher,
      AuthController.isClassAdmin,
    ],
    catchErrors(TeachersController.getStudents)
  )
  .post(
    [
      AuthController.isLoggedIn,
      AuthController.isTeacher,
      AuthController.isClassAdmin,
    ],
    catchErrors(TeachersController.addStudent)
  );

TeachersRouter.route("/me/results").post(
  [AuthController.isLoggedIn, AuthController.isTeacher],
  catchErrors(TeachersController.getResults)
);

TeachersRouter.route("/me/videos")
  .post(
    [AuthController.isLoggedIn, AuthController.isTeacher],
    catchErrors(TeachersController.uploadVideo)
  )
  .get(
    [AuthController.isLoggedIn, AuthController.isTeacher],
    catchErrors(TeachersController.getVideos)
  );

TeachersRouter.route("/students").post(
  [
    AuthController.isLoggedIn,
    AuthController.isTeacher,
    AuthController.isClassAdmin,
  ],
  catchErrors(TeachersController.getAllStudents)
);

TeachersRouter.route('/results/:id').delete(
  [AuthController.isLoggedIn, AuthController.isTeacher],
  catchErrors(TeachersController.deleteResult)
);

TeachersRouter.route("/students/:id").delete(
  [
    AuthController.isLoggedIn,
    AuthController.isTeacher,
    AuthController.isClassAdmin,
  ],
  catchErrors(TeachersController.deleteStudent)
);

TeachersRouter.route("/notes").get(
  [AuthController.isLoggedIn, AuthController.isTeacher],
  catchErrors(TeachersController.getAllMyNotes)
);

TeachersRouter.route('/submissions/:pid').put([AuthController.isLoggedIn, AuthController.isTeacher], catchErrors(TeachersController.setOption))

export default TeachersRouter;
