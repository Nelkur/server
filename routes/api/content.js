import express from "express";
import ContentController from "../../controllers/content.controller.js";
import AuthController from "../../controllers/auth.controller.js";
import UploadController from '../../controllers/uploads.controller.js';
import { catchErrors } from "../../handlers/errorHandler.js";
const ContentRouter = express.Router();

ContentRouter.route("/")
  .get(catchErrors(ContentController.getAll))
  .post(catchErrors(ContentController.getOne));

ContentRouter.route("/new").post(
  [AuthController.isLoggedIn, AuthController.isTeacher],
  UploadController.uploadPaperFiles(),
  catchErrors(ContentController.cleanUp),
  catchErrors(ContentController.addPaper)
);

export default ContentRouter;
