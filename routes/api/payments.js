import express from "express";
import PaymentsController from "../../controllers/payments.controller.js";
import AuthController from "../../controllers/auth.controller.js";
import { catchErrors } from "../../handlers/errorHandler.js";
const PaymentsRouter = express.Router();

PaymentsRouter.route("/pay").post(
  AuthController.isLoggedIn,
  catchErrors(PaymentsController.processRequest)
);

PaymentsRouter.route("/response/:subscription/:teachers?/:students?").post(
  catchErrors(PaymentsController.getResponse)
);

PaymentsRouter.route("/confirm").post(
  AuthController.isLoggedIn,
  catchErrors(PaymentsController.confirmPayment)
);

PaymentsRouter.route("/subscription").get(
  AuthController.isLoggedIn,
  catchErrors(PaymentsController.getSubscription)
);

PaymentsRouter.route("/all").get(
  AuthController.isLoggedIn,
  catchErrors(PaymentsController.getAllPayments)
);

export default PaymentsRouter;
