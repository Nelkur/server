import express from "express";
import YoutubeController from "../../controllers/youtube.controller.js";
import AuthController from "../../controllers/auth.controller.js";
import { catchErrors } from "../../handlers/errorHandler.js";
const YoutubeRouter = express.Router();

YoutubeRouter.route("/").get(catchErrors(YoutubeController.getChannel));

YoutubeRouter.route("/videos/:id").get(catchErrors(YoutubeController.getVideo));

YoutubeRouter.route("/upload").post(
  YoutubeController.upload(),
  catchErrors(YoutubeController.uploadVideo)
);

YoutubeRouter.route("/delete/:id").delete(
  catchErrors(YoutubeController.deleteVideo)
);

YoutubeRouter.route("/playlists")
  .get(catchErrors(YoutubeController.getPlaylist))
  .post(catchErrors(YoutubeController.createPlaylist))
  .delete(catchErrors(YoutubeController.deletePlaylist));

YoutubeRouter.route("/playlists/all/:channelId").get(
  YoutubeController.getAllPlaylists
);

YoutubeRouter.route("/playlists/:id/:etag?").get(
  catchErrors(YoutubeController.getPlaylist)
);

YoutubeRouter.route("/playlists/upload").post(
  catchErrors(YoutubeController.uploadVideoToPlaylist)
);

YoutubeRouter.route("/playlists/delete/:id").delete(
  catchErrors(YoutubeController.deleteVideoFromPlaylist)
);

YoutubeRouter.route("/playlists/items/:id").get(
  catchErrors(YoutubeController.getPlaylistItems)
);

export default YoutubeRouter;
