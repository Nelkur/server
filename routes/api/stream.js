import express from "express";
import StreamController from "../../controllers/stream.controller.js";
import AuthController from "../../controllers/auth.controller.js";
import { catchErrors } from "../../handlers/errorHandler.js";
const StreamRouter = express.Router();

StreamRouter.route("/signature").post(
  // AuthController.isLoggedIn,
  catchErrors(StreamController.generateSignature)
);

StreamRouter.route("/meeting").post(
  [AuthController.isLoggedIn, AuthController.isTeacher],
  catchErrors(StreamController.createMeeting)
);

export default StreamRouter;
