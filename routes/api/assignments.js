import express from "express";
import AssignmentController from "../../controllers/assignments.controller.js";
import AuthController from "../../controllers/auth.controller.js";
import UploadController from "../../controllers/uploads.controller.js";
import { catchErrors } from "../../handlers/errorHandler.js";
const AssignmentsRouter = express.Router();

AssignmentsRouter.route("/")
  .get(
    [AuthController.isLoggedIn, AuthController.isAdmin],
    catchErrors(AssignmentController.getAll)
  )
  .post(
    [AuthController.isLoggedIn, AuthController.isTeacher],
    UploadController.uploadAssignment(),
    catchErrors(AssignmentController.addNew)
  );

AssignmentsRouter.route("/teacher/:id").get(
  [AuthController.isLoggedIn, AuthController.isTeacher],
  catchErrors(AssignmentController.getByTeacher)
);

AssignmentsRouter.route("/student").get(
  [AuthController.isLoggedIn, AuthController.isStudent],
  catchErrors(AssignmentController.getStudentAssignments)
);

AssignmentsRouter.route('/status/:id').put(
  [AuthController.isLoggedIn, AuthController.isStudent],
  catchErrors(AssignmentController.updateAssignmentStatus)
)

AssignmentsRouter.route("/:id")
  .get(AuthController.isLoggedIn, catchErrors(AssignmentController.getOne))
  .put(
    [AuthController.isLoggedIn, AuthController.isTeacher],
    UploadController.uploadAssignment(),
    catchErrors(AssignmentController.updateAssignment)
  )
  .delete(
    [AuthController.isLoggedIn, AuthController.isTeacher],
    catchErrors(AssignmentController.deleteAssignment)
  );

export default AssignmentsRouter;
