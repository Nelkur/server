import express from "express";
import ProffersController from "../../controllers/proffers.controller.js";
import UploadController from "../../controllers/uploads.controller.js";
import AuthController from "../../controllers/auth.controller.js";
import { catchErrors } from "../../handlers/errorHandler.js";
const ProffersRouter = express.Router();

ProffersRouter.route("/student").get(
  [AuthController.isLoggedIn, AuthController.isStudent],
  catchErrors(ProffersController.getStudentSubmissions)
);

ProffersRouter.route('/student/classes').get(
  [AuthController.isLoggedIn, AuthController.isStudent],
  catchErrors(ProffersController.getClasses)
);

ProffersRouter.route('/student/:assignmentId').get(
  [AuthController.isLoggedIn, AuthController.isStudent],
  catchErrors(ProffersController.getSubmission)
);

ProffersRouter.route("/teacher/:id").get(
  [AuthController.isLoggedIn, AuthController.isTeacher],
  catchErrors(ProffersController.getTeacherSubmissions)
);

ProffersRouter.route("/new").post(
  [AuthController.isLoggedIn, AuthController.isStudent],
  UploadController.submitAssignment(),
  catchErrors(ProffersController.addSubmission)
);

ProffersRouter.route("/:id").put(
  [AuthController.isLoggedIn, AuthController.isTeacher],
  catchErrors(ProffersController.updateSubmission)
);

export default ProffersRouter;
