import express from "express";
import SubjectsController from "../../controllers/subjects.controller.js";
import { catchErrors } from "../../handlers/errorHandler.js";
const SubjectsRouter = express.Router();

SubjectsRouter.route("/")
  .get(catchErrors(SubjectsController.getAll))
  .post(catchErrors(SubjectsController.addNew));

export default SubjectsRouter;
