import express from "express";
import VideosController from "../../controllers/videos.controller.js";
import AuthController from "../../controllers/auth.controller.js";
import { catchErrors } from "../../handlers/errorHandler.js";
const VideosRouter = express.Router();

VideosRouter.route("/").get(catchErrors(VideosController.getAll));

VideosRouter.route("/institution/:schoolId").get(
  [AuthController.isLoggedIn, AuthController.isAdmin],
  catchErrors(VideosController.getBySchool)
);

VideosRouter.route("/institution/:schoolId/:cls").get(
  [AuthController.isLoggedIn, AuthController.isStudent],
  catchErrors(VideosController.getStudentVideos)
);

export default VideosRouter;
