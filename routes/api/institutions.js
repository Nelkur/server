import express from "express";
import InstitutionController from "../../controllers/institution.controller.js";
import AuthController from "../../controllers/auth.controller.js";
import UploadController from "../../controllers/uploads.controller.js";
import { catchErrors } from "../../handlers/errorHandler.js";
const InstitutionRouter = express.Router();

InstitutionRouter.route("/login").post(
  catchErrors(InstitutionController.login)
);

InstitutionRouter.route("/")
  .get(catchErrors(InstitutionController.getAll))
  .post(catchErrors(InstitutionController.addNew));

InstitutionRouter.route("/:slug")
  .get(
    [AuthController.isLoggedIn, AuthController.isAdmin], // For now all institution profiles are private
    catchErrors(InstitutionController.getOne)
  )
  .post(
    [AuthController.isLoggedIn, AuthController.isAdmin],
    UploadController.upload(),
    catchErrors(UploadController.resize),
    catchErrors(InstitutionController.uploadPhoto)
  )
  .put(
    [AuthController.isLoggedIn, AuthController.isAdmin],
    catchErrors(InstitutionController.updateInstitution)
  );

InstitutionRouter.route("/register").post(
  catchErrors(InstitutionController.addNew)
);

InstitutionRouter.route("/:slug/teachers")
  .get(
    [AuthController.isLoggedIn, AuthController.isAdmin],
    catchErrors(InstitutionController.getAllTeachers)
  )
  .post(
    [AuthController.isLoggedIn, AuthController.isAdmin],
    catchErrors(InstitutionController.addTeacher)
  );

InstitutionRouter.route("/:slug/teachers/:id")
  .put(
    [AuthController.isLoggedIn, AuthController.isAdmin],
    catchErrors(InstitutionController.updateTeacher)
  )
  .delete(
    [AuthController.isLoggedIn, AuthController.isAdmin],
    catchErrors(InstitutionController.deleteTeacher)
  );

InstitutionRouter.route("/:slug/grading")
  .get(
    [AuthController.isLoggedIn, AuthController.isAdmin],
    catchErrors(InstitutionController.getGrading)
  )
  .post(
    [AuthController.isLoggedIn, AuthController.isAdmin],
    catchErrors(InstitutionController.setGrading)
);
  
InstitutionRouter.route('/grading/:school').get(
    [AuthController.isLoggedIn, AuthController.isTeacher],
    catchErrors(InstitutionController.getGrading)
  )

InstitutionRouter.route("/:slug/students").get(
  [AuthController.isLoggedIn, AuthController.isAdmin],
  catchErrors(InstitutionController.getAllStudents)
);

InstitutionRouter.route("/content/:id").get(
  catchErrors(InstitutionController.getTeacherContent)
);

export default InstitutionRouter;
