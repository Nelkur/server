import express from "express";
import UploadController from "../../controllers/uploads.controller.js";
import NotesController from "../../controllers/notes.controller.js";
import AuthController from "../../controllers/auth.controller.js";
import { catchErrors } from "../../handlers/errorHandler.js";
const NotesRouter = express.Router();

NotesRouter.route("/")
  .get(catchErrors(NotesController.getAll))
  .post(
    [AuthController.isLoggedIn, AuthController.isTeacher],
    UploadController.uploadFile(),
    catchErrors(NotesController.addNew)
  );

NotesRouter.route("/:id")
  .get(AuthController.isLoggedIn, catchErrors(NotesController.getOne))
  .put(
    [AuthController.isLoggedIn, AuthController.isTeacher],
    catchErrors(NotesController.updateNotes)
  )
  .delete(
    [AuthController.isLoggedIn, AuthController.isTeacher],
    catchErrors(NotesController.deleteNotes)
  );

NotesRouter.route("/institution/:schoolId").get(
  [AuthController.isLoggedIn, AuthController.isAdmin],
  catchErrors(NotesController.getBySchool)
);

NotesRouter.route("/institution/:schoolId/:cls").get(
  [AuthController.isLoggedIn, AuthController.isStudent],
  catchErrors(NotesController.getStudentNotes)
);

export default NotesRouter;
