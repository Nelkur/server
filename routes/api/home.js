import express from "express";
const router = express.Router();

router.get("/", (req, res) => {
  res.render("index", { title: "Home" });
});

router.get("/oauth2callback", (req, res) => {
  const token = req.query.code;
  res.json({ token });
});

router.get("/reset", (req, res) => {
  res.render("email/password-reset", {
    resetURL:
      "http://localhost:5000/account/reset/teacher/53222b3bc4a808f2f8c586ed369390fd058d07aa",
  });
});

router.get("/invite-teacher", (req, res) => {
  res.render("email/invite-teacher", {
    resetUrl:
      "http://localhost:5000/account/invitation/teacher/53222b3bc4a808f2f8c586ed369390fd058d07aa",
  });
});

export default router;
