import Home from "./api/home.js";
import AuthRouter from "./api/auth.js";
import GradingRouter from "./api/grades.js";
import InstitutionRouter from "./api/institutions.js";
import TeachersRouter from "./api/teachers.js";
import StudentsRouter from "./api/students.js";
import IndividualRouter from "./api/individuals.js";
import SubjectsRouter from "./api/subjects.js";
import ContentRouter from "./api/content.js";
import NotesController from "./api/notes.js";
import PaymentsRouter from "./api/payments.js";
import YoutubeRouter from "./api/youtube.js";
import VideosRouter from "./api/videos.js";
import StreamRouter from "./api/stream.js";
import SubmissionsRouter from "./api/submissions.js";
import AssignmentsRouter from "./api/assignments.js";
import ProffersRouter from "./api/proffers.js";

export default function (app) {
  app.use("/", Home);
  app.use("/institutions", InstitutionRouter);
  app.use("/teachers", TeachersRouter);
  app.use("/students", StudentsRouter);
  app.use("/individuals", IndividualRouter);
  app.use("/subjects", SubjectsRouter);
  app.use("/grading", GradingRouter);
  app.use("/content", ContentRouter);
  app.use("/notes", NotesController);
  app.use("/payments", PaymentsRouter);
  app.use("/auth", AuthRouter);
  app.use("/youtube", YoutubeRouter);
  app.use("/videos", VideosRouter);
  app.use("/stream", StreamRouter);
  app.use("/submissions", SubmissionsRouter);
  app.use("/assignments", AssignmentsRouter);
  app.use("/proffers", ProffersRouter);
}
