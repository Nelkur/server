import express from "express";
import path from "path";
import morgan from "morgan";
// import moment from "moment";

import Cors from "./init/cors.js";
import Helpers from "./helpers/templateHelpers.js";
import Routes from "./routes/index.js";
import Logger from "./init/logger.js";
import * as ErrorHandler from "./handlers/errorHandler.js";
// import __dirname from "./utils/dirname.js";
/* ES modules don't support __dirname */
import { dirname } from "path";
import { fileURLToPath } from "url";
const __dirname = dirname(fileURLToPath(import.meta.url));

const app = express();

if (app.get("env") === "development") {
  // app.use(morgan("dev"));
} else {
  // Set up winston
  Logger();
}

Cors(app);

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

app.use(express.json());
app.use(express.static(path.join(__dirname, "public")));
app.use((req, res, next) => {
  res.locals.h = Helpers;
  // res.locals.moment = Helpers.moment();
  res.locals.siteName = "Elimu API";
  res.locals.currentPath = req.path;
  next();
});

// Set up routes
Routes(app);

// If none of the above routes could be matched 404 them and forward to error handler
app.use(ErrorHandler.notFound);

if (app.get("env") === "development") {
  /* Development Error Handler - Prints stack trace */
  app.use(ErrorHandler.developmentErrors);
}

app.use(ErrorHandler.productionErrors);

export default app;
