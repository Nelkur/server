import m from "moment";

class Helpers {
  constructor() {
    this.siteName = "Vidly";
    this.menu = [
      { slug: "/home", title: "Home", icon: "home" },
      { slug: "/api", title: "Api", icon: "api" },
    ];
  }

  static moment() {
    return m().format("MMMM Do YYYY");
  }

  static dump(obj) {
    return JSON.stringify(obj, null, 2);
  }
}

export default Helpers;
