import mongoose from "mongoose";
import dotenv from "dotenv";
dotenv.config({ path: ".env" });

// Import all our models
import "../models/Institutions.js";
import "../models/Teachers.js";
import "../models/Students.js";
import "../models/Individuals.js";
import "../models/Subjects.js";
import "../models/Grading.js";
import "../models/Content.js";
import "../models/Results.js";
import "../models/Subscriptions.js";
import "../models/Notes.js";
import "../models/Videos.js";
import "../models/Payments.js";
import "../models/Submissions.js";
import "../models/Assignments.js";
import '../models/Proffers.js';

export default function () {
  // Connect to our Database and handle any bad connections
  mongoose.set("useUnifiedTopology", true);
  mongoose.connect(process.env.DATABASE, {
    useNewUrlParser: true,
    useCreateIndex: true,
  });
  mongoose.Promise = global.Promise; // Tell Mongoose to use ES6 promises
  // mongoose.pluralize(null);
  mongoose.connection.on("error", (err) => {
    // logger.error(`🙅 🚫 🙅 🚫 🙅 🚫 🙅 🚫 → ${err.message}`);
    console.error(`🙅 🚫 🙅 🚫 🙅 🚫 🙅 🚫 → ${err.message}`);
  });
}
