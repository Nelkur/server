import winston from "winston";
import "winston-mongodb";

export default function () {
  const logger = winston.createLogger({
    level: "info",
    transports: [
      new transports.Console({
        format: winston.format.combine(
          winston.format.colorize(),
          winston.format.simple(),
          winston.format.prettyPrint()
        ),
        handleExceptions: true,
      }),
    ],
    exitOnError: false,
  });

  // Handle unhandled rejections and pass them to winston logger
  process.on("unhandledRejection", (err) => {
    throw err;
  });

  if (process.env.NODE_ENV === "production") {
    const fileLogger = new transports.File({ filename: "./logs/combined.log" });
    const exceptionsLogger = logger.exceptions.handle(
      new transports.File({ filename: "./logs/exceptions.log" })
    );
    const dbLogger = new transports.MongoDB({
      db: process.env.DATABASE,
      level: "error",
    });

    logger.clear().add(fileLogger).add(exceptionsLogger).add(dbLogger);
  }

  global.logger = logger;
}
