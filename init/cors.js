import cors from 'cors';

const whitelist = [ 'eschooling.co.ke' ];
const corsOptions = {
	origin: function(origin, callback) {
		if (whitelist.indexOf(origin) !== -1 || !origin) {
			callback(null, true);
		} else {
			callback(new Error('Not allowed by CORS'));
		}
	}
};

export default function(app) {
	if (process.env.NODE_ENV === 'production') {
		app.use(cors(corsOptions));
	} else {
		app.use(cors());
	}
}
